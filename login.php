<?php
$host       =   "localhost";
$user       =   "root";
$password   =   "";
$database   =   "lintasarta";
// perintah php untuk akses ke database
$koneksi = mysqli_connect($host, $user, $password, $database);

if(isset($_POST['login'])) {
	$username=$_POST['username'];
	$pass=$_POST['password'];
	$q=mysqli_query($koneksi,"SELECT username FROM user WHERE username='$username' AND password='$pass'");
	$j=mysqli_num_rows($q);
	if(empty($j)){
		echo "<div class='alert alert-danger'>Cek username dan password anda!</div>";
	}
	else {
		session_start();
		$_SESSION['username']=$username;
		header('location:index.php');
		echo "<div class='alert alert-danger'>Berhasil!</div>";
	}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>LOGIN</title>



    <link href="login/bootstraplogin.min.css" rel="stylesheet">


    <link href="login/metisMenu/metisMenu.min.css" rel="stylesheet">


    <link href="dist/css/sb-admin-2-login.css" rel="stylesheet">


    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="username" name="username" type="username" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>

                                <button type="submit" class="btn btn-lg btn-success btn-block" name="login">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="js/jquery.min.js"></script>


    <script src="js/bootstrap.min.js"></script>


    <script src="login/metisMenu/metisMenu.min.js"></script>


    <script src="js/sb-admin-2.js"></script>

</body>

</html>

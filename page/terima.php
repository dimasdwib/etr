<?php

// konfigurasi database
include "func.php";
session_start();
$lintas=new lintas;
$koneksi=$lintas->koneksi();
$posisi=$lintas->userToGroup($_SESSION['username']);
if ($posisi == 'PengujianVsat') {
	$jenis = 'V';
} elseif ($posisi == 'PengujianWireless') {
	$jenis = 'W';
}

//pengujian
if(isset($_POST['no_regis'])) {
	$no_regis = $_POST['no_regis'];
	foreach($no_regis as $value) {
		mysqli_query($koneksi,"INSERT INTO pengujian (no_regis) VALUES('$value')");
	}

	die(json_encode(['status' => 'berhasil']));
}
elseif ($_POST['t']=='hasil_uji') {
	$id_kat=$_POST['id_kat'];
	$q=mysqli_query($koneksi,"SELECT id_par,urutan FROM detail_kategori WHERE id_kat='$id_kat' ORDER BY urutan");
	while ($d=mysqli_fetch_row($q)){
		$id_par[]=$d[0]; $urutan[]=$d[1]; $par[]=$lintas->idparToParameter($d[0]);
	}
	$a=mysqli_query($koneksi,"SELECT noregis FROM batch INNER JOIN perangkat ON batch.kodespek = perangkat.kodespek WHERE batch.kategori='$id_kat' AND status_batch != 'uji' AND jenis='$jenis' ORDER BY id_batch DESC");
	while ($b=mysqli_fetch_row($a)) {
		$noregis[]=$b[0];
	}
	$data=array('id_par'=>$id_par, 'urutan'=>$urutan, 'parameter'=>$par, 'noregis'=>$noregis);
	echo json_encode($data);
}

if($_POST['t']=='tgl_uji'){
	$tanggal_uji=$_POST['search'];
	$tgl=$lintas->tglindo($tanggal_uji);
	$q=mysqli_query($koneksi,"SELECT pengujian.no_regis, info.tgl_uji, pengujian.id_par, pengujian.hasil_uji, info.penguji FROM info JOIN pengujian ON info.no_info=pengujian.no_info WHERE info.tgl_uji='$tgl' AND pengujian.id_par='7' GROUP BY pengujian.no_regis");
	while($d=mysqli_fetch_row ($q)){
		$no_regis[]=$d[0]; $tgl_uji[]=$d[1]; $hasil_uji[]=$d[3]; $penguji[]=$d[4];
	}
	if (empty($no_regis)) {
		$no_regis=''; $tgl_uji=''; $hasil_uji=''; $penguji='';
	}
	$data = array('no_regis'=>$no_regis,'tgl_uji'=>$tgl_uji,'hasil_uji'=>$hasil_uji,'penguji'=>$penguji);

	die(json_encode($data));
}

if($_POST['t']=='tgl_uji2'){
	$tanggal_uji=$_POST['search'];
	$q=mysqli_query($koneksi,"SELECT pengujian.no_regis, info.tgl_uji, pengujian.id_par, pengujian.hasil_uji, info.penguji FROM info JOIN pengujian ON info.no_info=pengujian.no_info WHERE pengujian.id_par='7' GROUP BY pengujian.no_regis");
	while($d=mysqli_fetch_row ($q)){
		$no_regis[]=$d[0]; $tgl_uji[]=$d[1]; $hasil_uji[]=$d[3]; $penguji[]=$d[4];
	}
	$data = array('no_regis'=>$no_regis,'tgl_uji'=>$tgl_uji,'hasil_uji'=>$hasil_uji,'penguji'=>$penguji);

	die(json_encode($data));
}

//repair
if(isset($_POST['t']) && $_POST['t']=='nama_form_rpr'){


	$y=$_POST['y']; $noregis_rpr = $_POST['a'];

		$query = mysqli_query($koneksi, "select nama_perangkat from perangkat where kodespek='$y'");
		$d=mysqli_fetch_row ($query);



	$data = array(
		  // 'j'=>$j,
		   'nama' => isset($d[0]) ? $d[0] : '',
		 );

		 die(json_encode($data));
}
//tanggal di riwayat repair
if(isset($_POST['t']) && $_POST['t']=='tanggal_rwyt'){
	$tanggal_riwayat=$_POST['search'];
	$tgl=$lintas->tglindo($tanggal_riwayat);
	$query = mysqli_query($koneksi, "select no_regis,tanggal,analisa,parts,id_user from repair where tanggal='$tgl'");
	while($d=mysqli_fetch_row ($query)){
		$no_regis[]=$d[0]; $tanggal[]=$d[1]; $analisa[]=$d[2]; $parts[]=$d[3]; $id_user[]=$d[4];
	}
	if (empty($no_regis)) {
		$no_regis=''; $tanggal=''; $analisa=''; $parts=''; $id_user='';
	}
	$data = array('no_regis'=>$no_regis,'tanggal'=>$tanggal,'analisa'=>$analisa,'parts'=>$parts,'id_user'=>$id_user);

	die(json_encode($data));
}

if(isset($_POST['t']) && $_POST['t']=='tanggal_rwyt2'){
	$tanggal_riwayat=$_POST['search'];
	$query = mysqli_query($koneksi, "select no_regis,tanggal,analisa,parts,id_user from repair");
	while($d=mysqli_fetch_row ($query)){
		$no_regis[]=$d[0]; $tanggal[]=$d[1]; $analisa[]=$d[2]; $parts[]=$d[3]; $id_user[]=$d[4];
	}
	$data = array('no_regis'=>$no_regis,'tanggal'=>$tanggal,'analisa'=>$analisa,'parts'=>$parts,'id_user'=>$id_user);

	die(json_encode($data));
}

//send ke db repair dr bttn send-rpr
if(isset($_POST['t']) && $_POST['t']=="noregis_rpr") {

	$noregis_repair=$_POST['noregis_rpr'];
	$nama=$_POST['nama_rpr'];
	$tanggal=$_POST['tanggal_rpr'];
	$analisa=$_POST['analisa'];
	$parts=$_POST['parts'];
	$ket=$_POST['ket'];

	//$c=count($noregis_repair);
	//for(i=0;i<$c;i++) {

	//}
	$i = 1;
	$data_pdf = [];
	foreach($noregis_repair as $key => $value) {
		if($_POST['no_draft'][$key] != '') {
			mysqli_query($koneksi,"DELETE FROM draft WHERE no_draft={$_POST['no_draft'][$key]}");
		}
		if ($value != '') {
			if ($tanggal[$key] != '') {
				$tgl = explode('-', $tanggal[$key]);
				$tgl = $tgl[2].'-'.$tgl[1].'-'.$tgl[0];
			} else {
				$tgl = '';
			}
			$tgl_kirim = date('Y-m-d');
			mysqli_query($koneksi,"INSERT INTO repair (no_regis,nama,tanggal,tanggal_kirim,analisa,parts,keterangan,id_user) VALUES('$value','$nama[$key]','$tgl','$tgl_kirim','$analisa[$key]','$parts[$key]','$ket[$key]','{$_SESSION['username']}')");
			$data_pdf[] = [$i, $value, $nama[$key], $tgl, $analisa[$key], $parts[$key] , $ket[$key]];
			$i++;
		}
	}
    $filename = 'repair_'.date('d-m-Y_s').".pdf";
	$pdf = new lintas();
	$pdf->repair_pdf($filename, $data_pdf);
	die(json_encode(['status' => 'berhasil', 'filename' => $filename]));
}

if(isset($_POST['action']) && $_POST['action']=='save_repair') {
	$noregis_rprs = $_POST['noregis_rpr'];
	foreach($noregis_rprs as $key => $noregis_rpr) {
		if ($noregis_rpr !== ''){
			$nama_rpr = $_POST['nama_rpr'][$key];
			if ($_POST['tanggal_rpr'][$key] != ''){
				$t = explode('-', $_POST['tanggal_rpr'][$key]);
				$tanggal_rpr = "$t[2]-$t[1]-$t[0]";
			}
			$analisa = $_POST['analisa'][$key];
			$parts = $_POST['parts'][$key];
			$ket = $_POST['ket'][$key];
			if ($_POST['no_draft'][$key] == ''){
				mysqli_query($koneksi,"INSERT INTO draft (noregis, nama, tanggal, analisa, parts, ket, iduser) VALUES('$noregis_rpr','$nama_rpr', '$tanggal_rpr', '$analisa','$parts','$ket','{$_SESSION['username']}' )");
			} else {
				mysqli_query($koneksi,"UPDATE draft SET noregis='$noregis_rpr', nama='$nama_rpr', tanggal='$tanggal_rpr', analisa='$analisa', parts='$parts', ket='$ket', iduser='{$_SESSION['username']}' WHERE no_draft='{$_POST['no_draft'][$key]}'");
			}
		}
	}

	die(json_encode(['status' => 'berhasil']));
}

//batch
if($_POST['t']=='scan'){

	$y=$_POST['y'];
    $noregis = $_POST['a'];
	//ambil data terakhir noregis, tanggal dan pukul
	$a=mysqli_query($koneksi, "select noregis from batch where noregis='$noregis' ");
	$j=mysqli_num_rows($a);

	//dibandingkan dengan data yang dikirim
	if($j < 1){
		$query = mysqli_query($koneksi, "select nama_perangkat,kategori from perangkat where kodespek='$y'");
		$d=mysqli_fetch_row($query);
        $kodespek=mysqli_num_rows($query);
        if ($kodespek > 0) {
		    mysqli_query($koneksi,"INSERT INTO batch (noregis,nama_perangkat,kodespek,kategori,status_batch,tanggal,pukul) VALUES('$noregis','$d[0]','$y','$d[1]','0',curdate(),curtime())");
		    $status='insert';
        } else {
            $status='noinsert';
        }
	} else {
		$status='noinsert';
	}

	$data = array('status'=>  $status,
		   'j'=>$j,
		   'nama' => isset($d[0]) ? $d[0] : '',
			);
  die(json_encode($data));

}

if($_POST['t'] == 'removebatch') {
	$koneksi = $lintas->koneksi();
	mysqli_query($koneksi, "DELETE from batch WHERE noregis='{$_POST['noregis']}'");
	die(json_encode(['status' => 'success']));
}

if( $_POST['t'] == 'update_kategori') {
	$koneksi = $lintas->koneksi();
	mysqli_query($koneksi, "UPDATE kategori SET kategori='{$_POST['nama_kategori']}' WHERE id_kat={$_POST['id_kategori']}");
	die(json_encode(['status' => 'berhasil']));
}

if($_POST['t'] == 'hapus_kategori') {
	$koneksi = $lintas->koneksi();
	mysqli_query($koneksi, "DELETE from kategori WHERE id_kat={$_POST['id_kategori']}");
	die(json_encode(['status' => 'berhasil']));
}

if( $_POST['t'] == 'update_parameter') {
	$koneksi = $lintas->koneksi();
	mysqli_query($koneksi, "UPDATE parameter SET parameter='{$_POST['nama_parameter']}' WHERE id_par={$_POST['id_parameter']}");
	die(json_encode(['status' => 'berhasil']));
}

if($_POST['t'] == 'hapus_parameter') {
	$koneksi = $lintas->koneksi();
	mysqli_query($koneksi, "DELETE from parameter WHERE id_par={$_POST['id_parameter']}");
	die(json_encode(['status' => 'berhasil']));
}
elseif($_POST['t']=="kategori_data_ambil") {
	$q=mysqli_query($koneksi,"SELECT id_kat,kategori FROM kategori ORDER BY kategori ASC");
	while($d=mysqli_fetch_row($q)) {

		$id[]=$d[0]; $kategori[]=$d[1];
	}
	$data=array('id'=>$id,'kategori'=>$kategori);
	echo json_encode($data);
}
elseif($_POST['t']=="kategori_nambah") {
	$kat=$_POST['kategori'];
	mysqli_query($koneksi,"INSERT INTO kategori (kategori) VALUES ('$kat')");
	$q=mysqli_query($koneksi,"SELECT id_kat,kategori FROM kategori ORDER BY kategori ASC");
	while($d=mysqli_fetch_row($q)) {
		$id[]=$d[0];
		$kategori[]=$d[1];
	}
	$data=array('id'=>$id,'kategori'=>$kategori);
	echo json_encode($data);
}

elseif($_POST['t']=="parameter_data_ambil") {
		$q=mysqli_query($koneksi,"SELECT id_par,parameter FROM parameter ORDER BY parameter ASC");
		while($d=mysqli_fetch_row($q)) {

			$id[]=$d[0]; $parameter[]=$d[1];
		}
		$data=array('id'=>$id,'parameter'=>$parameter);
		echo json_encode($data);
	}
elseif($_POST['t']=="parameter_nambah") {
		$par=$_POST['parameter'];
		mysqli_query($koneksi,"INSERT INTO parameter (parameter) VALUES ('$par')");
		$q=mysqli_query($koneksi,"SELECT id_par,parameter FROM parameter ORDER BY parameter ASC");
		while($d=mysqli_fetch_row($q)) {
			$id[]=$d[0];
			$parameter[]=$d[1];
		}
		$data=array('id'=>$id,'parameter'=>$parameter);
		echo json_encode($data);
	}
elseif ($_POST['t']=="pilih_detail") {
//	$pilihpar=$_POST['checkpar'];
	$pilihkat=$_POST['pilihkat'];
	$urutan=$_POST['urutan'];
//del det cat sesuai pilihkat
	mysqli_query($koneksi,"DELETE FROM detail_kategori WHERE id_kat='$pilihkat'");

	$c=count($urutan);
	for($d=0;$d<$c;$d++){

			$t = explode('-', $urutan[$d]);
			$no_urut=$t[0];
			$id_par=$t[1];

		if ($no_urut>0) {
			mysqli_query($koneksi,"INSERT INTO detail_kategori (id_kat,id_par,urutan) VALUES('$pilihkat','$id_par','$no_urut')");
		}
	}
	$q=mysqli_query($koneksi,"SELECT id_par,urutan FROM detail_kategori WHERE id_kat='$pilihkat'");
	while($d=mysqli_fetch_row($q)) {
		$cekpar[]=$d[0];
		$nomer[]=$d[1];

	}
	$data=array('pilihpar'=>$cekpar,'urutan'=>$nomer);
	echo json_encode($data);
}
elseif ($_POST['t']=="pilih_kat") {
	$pilihkat=$_POST['pilihkat'];
	$q=mysqli_query($koneksi,"SELECT id_par,urutan FROM detail_kategori WHERE id_kat='$pilihkat'");
	while ($d=mysqli_fetch_row($q)) {
		$gabung[]="$d[1]-$d[0]"; $id_par[]=$d[0];
	}
	$data=array('id_kat'=>$pilihkat,'gabung'=>$gabung, 'id_par'=>$id_par);
	echo json_encode($data);
}
elseif ($_POST['t']=='isi_info') {
	$asal=$_POST['asal'];
	$tgl_uji=$_POST['tgl_uji'];
	$status=$_POST['status'];
	$penguji=$_POST['penguji'];
	$appv=$_POST['appv'];
	$nama=$_POST['nama'];
	$batch_in=$_POST['batch_in'];
	$batch_out=$_POST['batch_out'];
	$id_kat=$_POST['id_kat'];

    $tgl_uji = explode('-', $tgl_uji);
    $batch_in= explode('-', $batch_in);
    $batch_out= explode('-', $batch_out);
    $tgl_uji = $tgl_uji[2].'-'.$tgl_uji[1].'-'.$tgl_uji[0];
    $batch_in = $batch_in[2].'-'.$batch_in[1].'-'.$batch_in[0];
    $batch_out = $batch_out[2].'-'.$batch_out[1].'-'.$batch_out[0];

	mysqli_query($koneksi,"INSERT INTO info(asal,tgl_uji,status_awal,penguji,appv,batch_in,batch_out,nama,id_kat) VALUES ('$asal','$tgl_uji','$status','$penguji','$appv','$batch_in','$batch_out','$nama','$id_kat')");
	$q=mysqli_query($koneksi,"SELECT no_info FROM info ORDER BY no_info DESC limit 1");
	$d=mysqli_fetch_row($q);
	$no_info=$d[0];

	$l=0;
	$valid=true;
	$noreglist = [];
	$q2=mysqli_query($koneksi,"SELECT noregis FROM batch WHERE kategori='$id_kat' AND status_batch != 'uji' order by id_batch desc");
	while ($d2=mysqli_fetch_row($q2)) {
		$noreglist[] = $d2;
		$noregis=$d2[0];

		$q3=mysqli_query($koneksi,"SELECT id_par FROM detail_kategori WHERE id_kat=$id_kat");
		while($d3=mysqli_fetch_row($q3)){
			$id_par=$d3[0];
			$par="uji$id_par";
			$hasil=$_POST[$par];
			if(empty($hasil[$l]) && $hasil[$l] != 0){
				mysqli_query($koneksi,"INSERT INTO pengujian(no_info,no_regis,id_par,hasil_uji,status) VALUES ('$no_info','$noregis','$id_par','','blm_uji')");
			} else {
				mysqli_query($koneksi,"INSERT INTO pengujian(no_info,no_regis,id_par,hasil_uji,status) VALUES ('$no_info','$noregis','$id_par','$hasil[$l]','uji')");
			}
		}

		$cek=mysqli_query($koneksi,"SELECT status FROM pengujian WHERE no_regis='$noregis' AND status='blm_uji'");
		$jml=mysqli_num_rows($cek);
		if($jml == 0) {
			mysqli_query($koneksi,"UPDATE batch SET status_batch='uji' WHERE noregis='$noregis'");
		}
		else {
			$valid = false;
			mysqli_query($koneksi,"DELETE FROM pengujian where no_regis='$noregis'");
			mysqli_query($koneksi,"DELETE FROM info where no_info='$no_info'");
		}

		$l+=1;
	}


	if ($valid == false) {
		foreach ($noreglist as $no) {
			mysqli_query($koneksi,"UPDATE batch SET status_batch='0' WHERE noregis='$no[0]'");
			mysqli_query($koneksi,"DELETE FROM pengujian where no_regis='$no[0]'");
		}
		$data= array('msg' => 'data harus diisi semua', 's' => 'info_gagal', 'id_kat'=>$id_kat);
		die(json_encode($data));
	}


	if(mysqli_affected_rows($koneksi) > 0) {
        $s='info_masuk';
    } else {
        $s='info_gagal';
    }

	$data= array('s' => $s, 'id_kat'=>$id_kat);
	echo json_encode($data);
}
elseif ($_POST['t']=='list_pengujian') {
	// $a = mysqli_query($koneksi, "SELECT tanggal,noregis,nama_perangkat,status_batch FROM batch WHERE status_batch !='uji' ");
	$posisi = $lintas->userToGroup($_SESSION['username']);
  if ($posisi == 'PengujianVsat') {
  	$jenis = 'V';
  } elseif ($posisi == 'PengujianWireless') {
  	$jenis = 'W';
  }
  $a = mysqli_query($koneksi, "SELECT tanggal, noregis, nama_perangkat, status_batch FROM batch INNER JOIN perangkat ON batch.kodespek = perangkat.kodespek WHERE status_batch != 'uji' AND jenis='$jenis'");
	while($d = mysqli_fetch_row($a)) {
		$tanggal[]=$d[0]; $noregis[]=$d[1]; $nama_perangkat[]=$d[2];
	}
	$data=array('tanggal'=>$tanggal, 'noregis'=>$noregis, 'nama_perangkat'=>$nama_perangkat);
	echo json_encode($data);
}


elseif( $_POST['t'] == 'update_user') {
	mysqli_query($koneksi, "UPDATE user SET username='$_POST[username]',nama_user='$_POST[nama]',posisi='$_POST[posisi]' WHERE username='$_POST[username]'");
	$q=mysqli_query($koneksi, "SELECT username,nama_user,posisi FROM user ORDER by username ASC");
	while($d = mysqli_fetch_row($q)) {
	$username[]=$d[0]; $nama2[]=$d[1]; $posisi2[]=$d[2];
	}
	$data=array('username'=>$username, 'nama_user'=>$nama2, 'posisi'=>$posisi2);
	echo json_encode($data);

}
elseif($_POST['t']=="tambah_user") {
	$username=$_POST['username'];
	$password=md5($_POST['password']);
	$nama_user=$_POST['nama_user'];
	$posisi=$_POST['posisi'];
	mysqli_query($koneksi,"INSERT INTO user (username,password,nama_user,posisi) VALUES ('$username','$password','$nama_user','$posisi')");
	$q=mysqli_query($koneksi, "SELECT username,nama_user,posisi FROM user ORDER by username ASC");
	while($d = mysqli_fetch_row($q)) {
	$username2[]=$d[0]; $nama_user2[]=$d[1]; $posisi2[]=$d[2];
	}
	$data=array('username'=>$username2, 'nama_user'=>$nama_user2, 'posisi'=>$posisi2);
	echo json_encode($data);

}

if ($_POST['t'] == "get_riwayat_pengujian") {
	$q=mysqli_query($koneksi,"SELECT pengujian.no_regis, info.tgl_uji, pengujian.id_par, pengujian.hasil_uji, info.penguji FROM info JOIN pengujian ON info.no_info=pengujian.no_info WHERE pengujian.id_par='7' GROUP BY pengujian.no_regis");
	$no = 0;
	$data = "";
	while($d=mysqli_fetch_row($q)) {
		 $no++;
		 $data .= "<tr><td>$no</td><td>$d[0]</td><td>$d[1]</td><td>$d[3]</td><td>$d[4]</td>
			 </tr>";
	}
	die($data);
}

if ($_POST['t'] == "get_riwayat_repair") {
    $q=mysqli_query($koneksi,"SELECT * FROM repair ");
    $no=0;
    $result = "";
    while($d=mysqli_fetch_row($q)) {
    $no++;
    $result .= "<tr><td>$no</a></td><td>$d[1]</td><td>$d[3]</td><td>$d[5]</td><td>$d[6]</td>
        <td>$d[8]</td>
      </tr>";
    }
    die($result);
}

if ($_POST['t'] == "get_pilih_kategori") {
    $q=mysqli_query($koneksi,"SELECT id_kat,kategori FROM kategori ORDER BY kategori ASC") or die("gagal");
    $res = '<option value="">Pilih Kategori</option>';
    while($d=mysqli_fetch_row($q)) {
      $res .= "<option value=$d[0]>$d[1]</option>";
    }
    die($res);
}

if ($_POST['t'] == "get_pilih_parameter") {
    $q=mysqli_query($koneksi,"SELECT id_par,parameter FROM parameter ORDER BY parameter ASC");
    $res = '';
    while($d=mysqli_fetch_row($q)) {
      $arr= array(0,1,2,3,4,5,6,7,8,9);
      $res .= "<div class='col-md-4'>
              <select name=urutan[] data-id=$d[0]>";
      foreach ($arr as $value) {
        $res .= "<option value='$value-$d[0]'>$value</option>";
      }
      $res .= "</select> $d[1]<br></div>";
    }
    die($res);
}

$action = isset($_POST['t']) ? $_POST['t'] : null;

/*=============  User manajemen  ==============*/
if ($action == 'get_users') {
	$query = mysqli_query($koneksi, "Select * from user order by username");
    $data = [];
    while($d=mysqli_fetch_assoc($query)) {
		 $data[] = $d;
	}
    die(json_encode($data));
}

if ($action == 'hapus_user') {
    $id = $_POST['id'];
	mysqli_query($koneksi, "DELETE from user WHERE id_user=$id");
    if(mysqli_affected_rows($koneksi)>0) {
        die(json_encode(['status' => 'success']));
    } else {
        die(json_encode(['status' => 'failed']));
    }
}

if ($action == 'update_profile') {

    $username = strip_tags($_POST['username']);
    $nama_user = strip_tags($_POST['nama_user']);
    $password = strip_tags($_POST['old_password']);

    if ($password != '') {
        $newpassword = strip_tags($_POST['new_password']);
        $confirmnewpassword = strip_tags($_POST['con_password']);
    	if (strlen($newpassword) < 6) {
    		$res = [
    			'status' => 'failed',
                'error' => 'new_password',
    			'message'=> 'Password minimal 6 karakter',
    		];
    		die(json_encode($res));
    	}
        $password = md5($password);
		$q = mysqli_query($koneksi,"SELECT * FROM user WHERE username='{$username}' AND password='{$password}'");
		$user = mysqli_fetch_assoc($q);
    } else {
        $q = mysqli_query($koneksi,"SELECT * FROM user WHERE username='{$username}'");
		$user = mysqli_fetch_assoc($q);
    }

		if(isset($user['id_user'])) {
            if ($password != '') {
                $newpassword = md5($newpassword);
    			mysqli_query($koneksi,"UPDATE user SET nama_user='$nama_user', password='$newpassword' WHERE username='{$username}'");
            } else {
                mysqli_query($koneksi,"UPDATE user SET nama_user='$nama_user' WHERE username='{$username}'");
            }
            $res = [
                'username' => $username,
                'nama_user' => $nama_user,
				'status' => 'success',
				'message'=> 'Profil berhasil diperbarui',
			];
            $_SESSION['name'] = $nama_user;
			die(json_encode($res));
		}

		$res = [
			'status' => 'failed',
            'error' => 'old_password',
			'message'=> 'Password lama salah',
		];
		die(json_encode($res));
}
/*=============  User manajemen  ==============*/

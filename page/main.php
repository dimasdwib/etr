<?php

$page = isset($_GET['page']) ? $_GET['page'] : null;

switch ($page) {
  case 'batch':
    require_once 'batch.php';
    break;
  case 'pengujian':
    require_once 'listpengujian.php';
    break;
  case 'detailpengujian':
    require_once 'detailpengujian.php';
    break;
  default:
    require_once 'dashboard.php';
    break;
}

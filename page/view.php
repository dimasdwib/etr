<?php
function batch(){
?>
  <div class="col-md-12" id="batch">
    <h3><i class="fa fa-barcode fa-fw"></i> Batch In </h3>
    <hr />
    <div id="buttonbatchh">
      <button type="button" id="btn-send" class="btn btn-warning">Send</button>
    </div>
    <BR>
    <div class="table-responsive">
      <div id="scan">
          <form id="table-batch-form">
            <div class="panel panel-default">
            <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>No Registrasi</th>
                        <th>Nama</th>
                        <th colspan="2">Kode Spek</th>
                    </tr>
                </thead>
                <tbody id="table-batch">
                    <tr>
                        <td></td>
                        <td><input type="text" class="form-control" size="18" id="noregis"></td>
                        <td><input type="text" class="form-control" readonly id="nama" size="50"></td>
                        <td colspan="2"><input type="text" class="form-control" readonly id="kodespek" size="15"></td>
                    </tr>
                </tbody>
            </table>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
}
?>

<?php
function getinfo(){
  global $lintas;
?>
<script>
$(".datepicker").datepicker({
 dateFormat: 'dd-mm-yy'
});
</script>
<div class="col-md-12" id="info">
  <form id="table-pengujian">
  <h3> Info </h3>
  <hr>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <td>Asal Perangkat</td>
          <td width="200"><input type="text" placeholder="Asal Perangkat" class="form-control" name="asal"></td>
          <td width="200">Pengujian</td>
          <td width="200"><input type="text" class="form-control" name="penguji" readonly value="<?php echo $_SESSION['name'] ?>"></td>
          <td>Batch In</td>
          <td><input class="form-control datepicker" value="<?=date("d-m-Y")?>" readonly name="batch_in"></td>
        </tr>
        <tr>
          <td>Tanggal Uji</td>
          <td><input class="form-control datepicker" value="<?=date("d-m-Y")?>" readonly name="tgl_uji"></td>
          <td>Appv. Data Entry</td>
          <td><input type="text" class="form-control" placeholder="Appv. Data Entry" name="appv"></td>
          <td>Batch Out</td>
          <td><input class="form-control datepicker" value="<?=date("d-m-Y")?>" name="batch_out"></td>
        </tr>
        <tr>
          <td>Status Awal</td>
          <td><select class="form-control" name="status">
            <option>Baru</option>
            <option>Dismantle</option>
            <option>Repair</option>
          </select></td>
          <td colspan="2">Nama Perangkat</td>
          <td colspan="2"><input type="text" class="form-control" placeholder="Nama perangkat" name="nama"></td>
        </tr>
      </table>
    </div>
  </div>
  <div class="col-md-12">
  <h3> Standar Nilai Uji </h3>
  <hr>
  <div class="row standar" id="standar1">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th colspan="2">Kategori Kerusakan</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <td>Tidak Cacat / Mulus</td>
          <td>5. Center LO (SG)</td>
          <td>1.110Mhz+/-1%</td>
          <td>1.Tx problem</td>
          <td>5.Connector Problem</td>
        </tr>
        <tr>
          <td>2. Gain BUC 2W (SW)</td>
          <td>45dB @ Tx modem -40dBm</td>
          <td colspan="2">SW (software Power Meter)</td>
          <td>2.Rx problem</td>
          <td>6.Fisik Problem</td>
        </tr>
        <tr>
          <td>3. Gain BUC 2W (SW)</td>
          <td>33dB @ Tx modem -22dBm</td>
          <td colspan="2">SA (Spectrum Analyzer)</td>
          <td>3.Interface Problem</td>
          <td>7.Software Problem</td>
        </tr>
        <tr>
          <td>4. Gain BUC 2W (SW)</td>
          <td>50dB @ Tx modem -22dBm</td>
          <td colspan="2">SG (Synthesizer Generator)</td>
          <td>4.Power Problem</td>
          <td>8.Other</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row standar" id="standar2">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th colspan="2">Rules</th>
          <th>Parameter Uji</th>
          <th colspan="3">Rules</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <td colspan="2">Tidak Cacat / Mulus</td>
          <td>3. Gain </td>
          <td colspan="3">10W - 120W:(65dB s/d 85dB)</td>
        </tr>
        <tr>
          <td rowspan="4">2. LED Indicator</td>
          <td>Online</td>
          <td>ON</td>
          <td>4. Power</td>
          <td>10W:40dBm 20W:43dBm</td>
          <td colspan="2">10W:46dBm 60W:49dBm 100W:50dBm</td>
        </tr>
        <tr>
          <td>Transmit</td>
          <td>ON</td>
          <th colspan="4">Kategori Kerusakan(KK)</th>
        </tr>
        <tr>
          <td>Receive</td>
          <td>ON</td>
          <td>1. Tx Problem</td>
          <td>3. Interface Problem</td>
          <td>5. Fisik Problem</td>
          <td>7. Power Problem</td>
        </tr>
        <tr>
          <td>Fault(RTF)</td>
          <td>OFF</td>
          <td>2. Rx Problem</td>
          <td>4. Connector Problem</td>
          <td>5. Software Problem</td>
          <td>7. Other</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row standar" id="standar3">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th colspan="2">Rules</th>
          <th colspan="2">Parameter Uji</th>
          <th colspan="4">Rules</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <th colspan="2">Tidak Cacat / Mulus</th>
          <td colspan="2">3. SQF</td>
          <th colspan="2"> &lt; 45(HX,HT)</th>
          <th colspan="2"> &lt; 70(iDirect,X1,X3)</th>
        </tr>
        <tr>
          <td rowspan="4">2. LED Indicator</td>
          <td colspan="2"></td>
          <td colspan="2">4. Ping</td>
          <th colspan="2">RTO</th>
          <th colspan="2">not RTO</th>
        </tr>
        <tr>
          <th>LAN</th>
          <td>ON</td>
          <th>System</th>
          <td>ON</td>
          <th colspan="4">Kategori Kerusakan(KK)</th>
        </tr>
        <tr>
          <th>Transmit</th>
          <td>ON</td>
          <th>Power</th>
          <td>ON</td>
          <td>1. Tx Problem</td>
          <td>3. Interface Problem</td>
          <td>5. Fisik Problem</td>
          <td>7. Power Problem</td>
        </tr>
        <tr>
          <th>Receive</th>
          <td colspan="3">ON</td>
          <td>2. Rx Problem</td>
          <td>4. Connector Problem</td>
          <td>6. Software Problem</td>
          <td>8. Other</td>
        </tr>
      </table>
    </div>
  </div>
  </div>

  <div class="row standar" id="standar4">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th colspan="3">STANDAR UJI</th>
          <th colspan="2">Kategori Kerusakan(KK)</th>
        </tr>
        <tr>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <td rowspan="5">*ip system WiBas : 10.10.10.100</td>
          <td>1. Tx Problem</td>
          <td>6. Fisik Problem</td>
        </tr>
        <tr>
          <th>1. Fisik</th>
          <th>Tidak Cacat / Mulus</th>
          <td>2. Rx Problem</td>
          <td>7. Software Problem</td>
        </tr>
        <tr>
          <th>2. Factory Default</th>
          <th>Mandatory</th>
          <td>3. Interface Problem</td>
          <td>8. Other</td>
        </tr>
        <tr>
          <th>3. Ping IP System</th>
          <th>No RTO*</th>
          <td>4. Power Problem</td>
        </tr>
        <tr>
          <th>4. Software Version</th>
          <th>1.9</th>
          <td>5. Connector Problem</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row standar" id="standar5">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th colspan="3">Kategori Kerusakan</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <th>OK</th>
          <td>4. Interface</td>
          <th>Bert 300 second Free Error</th>
          <td>1. TX Problem</td>
          <td>4. Power Problem</td>
          <td>7. Software Problem</td>
        </tr>
        <tr>
          <td>2. Power</td>
          <th>On</th>
          <td>5. Tx</td>
          <th>-30 dBm s/d -25 dBm</th>
          <td>2. RX Problem</td>
          <td>5. Connector Problem</td>
          <td rowspan="2">8. Other</td>
        </tr>
        <tr>
          <td>3. Link</td>
          <th>Connected</th>
          <td>6. Rsl</td>
          <th>-84 dBm s/d -70 dBm</th>
          <td>3. Interface Problem</td>
          <td>6. Fisik Problem</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row standar" id="standar6">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th colspan="2">Kategori Kerusakan</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <th>OK</th>
          <td>1. TX Problem</td>
          <td>5. Connector Problem</td>
        </tr>
        <tr>
          <td>2. Power</td>
          <th>On</th>
          <td>2. RX Problem</td>
          <td>6. Fisik Problem</td>
        </tr>
        <tr>
          <td>3. Vout</td>
          <th>-48.96 Vdc s/d -47.04 Vdc</th>
          <td>3. Interface Problem</td>
          <td>7. Software Problem</td>
        </tr>
        <tr>
          <td>4. Vout(max)</td>
          <th> <= 360 mV</th>
          <td>4. Power Problem</td>
          <td>8. Other</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row standar" id="standar7">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th colspan="3">Kategori Kerusakan</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <th>OK</th>
          <td>4. Interface</td>
          <th>Bert 300 second Free Error</th>
          <td>1. TX Problem</td>
          <td>4. Power Problem</td>
          <td>7. Software Problem</td>
        </tr>
        <tr>
          <td>2. Power</td>
          <th>On</th>
          <td>5. SNR</td>
          <th> >= 23 dB</th>
          <td>2. RX Problem</td>
          <td>5. Connector Problem</td>
          <td rowspan="2">8. Other</td>
        </tr>
        <tr>
          <td>3. Link</td>
          <th>Connected</th>
          <td>6. Tx/Rx</td>
          <th> &lt;-30/>-60 </th>
          <td>3. Interface Problem</td>
          <td>6. Fisik Problem</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row standar" id="standar8">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th colspan="3">Kategori Kerusakan</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <th>OK</th>
          <td>4. Ethernet</td>
          <th>UP, Test ping Reply</th>
          <td>1. TX Problem</td>
          <td>4. Power Problem</td>
          <td>7. Software Problem</td>
        </tr>
        <tr>
          <td>2. Power</td>
          <th>On</th>
          <td>5. UL/DL SNR</td>
          <th> >23 dBm</th>
          <td>2. RX Problem</td>
          <td>5. Connector Problem</td>
          <td rowspan="2">8. Other</td>
        </tr>
        <tr>
          <td>3. Link</td>
          <th>Connected</th>
          <td>6. UL/DL Rssi</td>
          <th> >-85 dBm </th>
          <td>3. Interface Problem</td>
          <td>6. Fisik Problem</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row standar" id="standar9">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th colspan="3">Kategori Kerusakan</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <th>OK</th>
          <td>4. Ethernet</td>
          <th>UP, Test ping Reply</th>
          <td>1. TX Problem</td>
          <td>4. Power Problem</td>
          <td>7. Software Problem</td>
        </tr>
        <tr>
          <td>2. Power</td>
          <th>On</th>
          <td>5. CINR</td>
          <th> >23 dBm</th>
          <td>2. RX Problem</td>
          <td>5. Connector Problem</td>
          <td rowspan="2">8. Other</td>
        </tr>
        <tr>
          <td>3. Link</td>
          <th>Connected</th>
          <td>6. Rssi</td>
          <th> >-85 dBm </th>
          <td>3. Interface Problem</td>
          <td>6. Fisik Problem</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row standar" id="standar10">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th colspan="3">Kategori Kerusakan</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <th>OK</th>
          <td>4. Interface</td>
          <th>Serial Up, line protocol Up</th>
          <td>1. TX Problem</td>
          <td>4. Power Problem</td>
          <td>7. Software Problem</td>
        </tr>
        <tr>
          <td>2. Power</td>
          <th>On</th>
          <td>5. Tx</td>
          <th> Up, Test ping reply</th>
          <td>2. RX Problem</td>
          <td>5. Connector Problem</td>
          <td rowspan="2">8. Other</td>
        </tr>
        <tr>
          <td>3. Link</td>
          <th>Connected</th>
          <td>6. Rsl</td>
          <th>Voice On</th>
          <td>3. Interface Problem</td>
          <td>6. Fisik Problem</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row standar" id="standar11">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th colspan="3">Kategori Kerusakan</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <th>OK</th>
          <td>4. Interface</td>
          <th>Bert 300 second Free Error</th>
          <td>1. TX Problem</td>
          <td>4. Power Problem</td>
          <td>7. Software Problem</td>
        </tr>
        <tr>
          <td>2. Power</td>
          <th>On</th>
          <td>5. Tx Power</td>
          <th> -15 s/d 25 dBm</th>
          <td>2. RX Problem</td>
          <td>5. Connector Problem</td>
          <td rowspan="2">8. Other</td>
        </tr>
        <tr>
          <td>3. Link</td>
          <th>Connected</th>
          <td>6. Rx</td>
          <th> >-65 dBm </th>
          <td>3. Interface Problem</td>
          <td>6. Fisik Problem</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="row standar" id="standar12">
    <div class="col-md-12">
      <table class="table table-bordered">
        <tr>
          <th>Parameter Uji</th>
          <th>Rules</th>
          <th colspan="2">Kategori Kerusakan</th>
        </tr>
        <tr>
          <td>1. Fisik</td>
          <th>OK</th>
          <td>1. TX Problem</td>
          <td>5. Connector Problem</td>
        </tr>
        <tr>
          <td>2. Power</td>
          <th>On</th>
          <td>2. RX Problem</td>
          <td>6. Fisik Problem</td>
        </tr>
        <tr>
          <td>3. Link</td>
          <th>Connected</th>
          <td>3. Interface Problem</td>
          <td>7. Software Problem</td>
        </tr>
        <tr>
          <td>4. Interface</td>
          <th>Bert 300 second free Error</th>
          <td>4. Power Problem</td>
          <td>8. Other</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="col-md-12" id="tabel_uji">
      <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped">
              <thead>

              </thead>
              <tbody></tbody>
          </table>
      </div>
  </div>

      <div class="col-md-12">
          <div class="form-group">
              <button type="button" class="btn btn-primary pull-right"> Save and submit </button>
          </div>
      </div>
  </form>
</div>

  <?php
}

?>

<?php
function pengujian(){
?>
  <div class="col-md-12" id="pengujian">
    <h3><i class="fa fa-file-text-o fa-fw"></i> Pengujian</h3>
    <hr>
      <div class="table-responsive" id="tbuji">
        <table class="table table-hover">
          <thead>
              <tr>
                  <th>No.</th>
                  <th>Tanggal</th>
                  <th>No Registrasi</th>
                  <th>Nama</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody id="datauji">
              <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td><a href="isipengujian"><i class="fa fa-cog" aria-hidden="true"></i></a></td>
              </tr>
          </tbody>
        </table>
      </div>
  </div>
<?php
 }

 ?>

<?php
function riwayat_pengujian(){
  $lintas=new lintas;
  $koneksi=$lintas->koneksi();
?>
<div class="col-md-12">
  <form id="riwayat_pengujian">
  <h3><i class="fa fa-clipboard fa-fw"></i> Riwayat Pengujian</h3>
  <hr />
  <div class="form-group form-inline">
    <label for="tanggal_uji">Search by Date:  </label>
      <input type="text" class="datepicker form-control" name="search" id="tanggal_uji">
  </div>
  <div class="panel panel-default">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No.</th>
              <th>No Registrasi</th>
              <th>Tanggal</th>
              <th>Hasil Uji</th>
              <th>Penguji</th>
            </tr>
          </thead>
          <tbody id="tbody_riwayat_pengujian">
          <?php
         $q=mysqli_query($koneksi,"SELECT pengujian.no_regis, info.tgl_uji, pengujian.id_par, pengujian.hasil_uji, info.penguji FROM info JOIN pengujian ON info.no_info=pengujian.no_info WHERE pengujian.id_par='7' GROUP BY pengujian.no_regis");
         $no=0;
    while($d=mysqli_fetch_row($q)) {
          $no++;
      echo "<tr><td>$no</td><td>$d[0]</td><td>$d[1]</td><td>$d[3]</td><td>$d[4]</td>
            </tr>";
    }
    ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
  </form>
</div>

<script>
 $(".datepicker").datepicker({
   dateFormat: 'dd-mm-yy'
 });
</script>
<?php
}
?>

<?php
function dashboard() {
    $lintas=new lintas;
    $koneksi=$lintas->koneksi();
    $posisi=$lintas->userToGroup($_SESSION['username']);
    if ($posisi == 'PengujianVsat') {
    	dashboard_pengujian();
    } elseif ($posisi == 'PengujianWireless') {
    	dashboard_pengujian();
    } elseif ($posisi == 'JuniorManager' || $posisi == 'SeniorEngineer') {
    	dashboard_manager();
    } elseif ($posisi == 'Repair') {
        dashboard_repair();
    }
}
?>

<?php
function dashboard_repair() {
$lintas=new lintas;
$koneksi=$lintas->koneksi();
$posisi=$lintas->userToGroup($_SESSION['username']);
$bulan= ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];
$i = 1;
$data = [];
foreach ($bulan as $value) {
    $d = mysqli_query($koneksi, "select * from repair where MONTH(tanggal)={$i} AND id_user='{$_SESSION['username']}'");
    $data[] = [
        'month' => $value,
        'value' => mysqli_num_rows($d),
    ];
    $i++;
}
$year_now = date('Y');
$target = 60 * 12;
$p = mysqli_query($koneksi, "select * from repair where YEAR(tanggal)={$year_now} AND id_user='{$_SESSION['username']}'");
$pre = mysqli_num_rows($p);
$current_repair = ($pre / $target) * 100;
$target_repair = 100 - $current_repair;
?>
    <div class="col-md-12" id="dashboard">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default" style="height:330px;">
                    <div class="panel-heading"><i class="fa fa-smile-o"></i> Greetings </div>
                    <div class="panel-body">
                        <h3 style="margin: 5px 0">Welcome</h3>
                        <b><?=$_SESSION['name']?></b>
                        </br>
                        </br>
                        <h3 style="margin: 5px 0">Posisi</h3>
                        <b><?=$posisi?></b>
                        </br>
                        </br>
                        <h5><?= date('D, H:i:s') ?></h5>
                        <h3 style="margin:0"><?= date('d-M-Y') ?> </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default" style="height:330px;">
                    <div class="panel-heading"><i class="fa fa-pie-chart"></i> Statistic </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-7">
                                <div id="piechart" style="height:250px;"></div>
                            </div>
                            <div class="col-md-5">
                                <h4> Detail </h4>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Target</b><span class="badge"><?=60 * 12 ?></span></li>
                                    <li class="list-group-item"><b>Repair</b><span class="badge"><?=$pre?></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="barchart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>
    <script>
    var chartData = [];
    $.each(<?=json_encode($data)?>, function(index, d) {
        chartData.push(d.value);
    });
     Highcharts.chart('barchart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Repair Statistic',
            style: { "fontSize": "0.9em", "margin": "2px" }
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        series: [{
            type: 'column',
            name: 'Repair',
            data: chartData
        }, {
            type: 'spline',
            name: 'Target',
            data: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60]
        }]
     });

     Highcharts.chart('piechart', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Repair Statistic',
            style: { "display": "none" },
            floating: true
        },
        series: [{
            name: 'Repair Statistic',
            data: [
                {
                    name: 'Repair',
                    y: <?=$current_repair?>,
                    selected: true
                },
                {
                    name: 'Target',
                    y: <?=$target_repair?>
                }
            ]
        }]
     });
    </script>
<?php
}
?>

<?php
function dashboard_pengujian() {
$lintas=new lintas;
$koneksi=$lintas->koneksi();
$posisi=$lintas->userToGroup($_SESSION['username']);
if ($posisi == 'PengujianVsat') {
	$jenis = 'V';
} elseif ($posisi == 'PengujianWireless') {
	$jenis = 'W';
}
$bulan= ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];
$i = 1;
$data = [];
foreach ($bulan as $value) {
    $d = mysqli_query($koneksi, "select pengujian.* from pengujian INNER JOIN info ON pengujian.no_info = info.no_info WHERE MONTH(tgl_uji)={$i} AND penguji='{$_SESSION['username']}' GROUP BY no_regis");
    $data[] = [
        'month' => $value,
        'value' => mysqli_num_rows($d),
    ];
    $i++;
}
$year_now = date('Y');
$target = 60 * 12;
$p = mysqli_query($koneksi, "select pengujian.* from pengujian INNER JOIN info ON pengujian.no_info = info.no_info WHERE YEAR(tgl_uji)={$year_now} AND penguji='{$_SESSION['username']}' GROUP BY no_regis");
$pre = mysqli_num_rows($p);
$current_repair = ($pre / $target) * 100;
$target_repair = 100 - $current_repair;

?>
    <div class="col-md-12" id="dashboard">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default" style="height:330px;">
                    <div class="panel-heading"><i class="fa fa-smile-o"></i> Greetings </div>
                    <div class="panel-body">
                        <h3 style="margin: 5px 0">Welcome</h3>
                        <b><?=$_SESSION['name']?></b>
                        </br>
                        </br>
                        <h3>Posisi</h3>
                        <b><?=$posisi?></b>
                        </br></br>
                        <h5><?= date('D, H:i:s') ?></h5>
                        <h3 style="margin:0"><?= date('d-M-Y') ?> </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="panel panel-default" style="height:330px;">
                    <div class="panel-heading"><i class="fa fa-pie-chart"></i> Statistic </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-7">
                                <div id="piechart" style="height:250px;"></div>
                            </div>
                            <div class="col-md-5">
                                <h4> Detail </h4>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Target</b><span class="badge"><?=60 * 12?></span></li>
                                    <li class="list-group-item"><b>Pengujian</b><span class="badge"><?=$pre?></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="barchart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>
    <script>
    var chartData = [];
    $.each(<?=json_encode($data)?>, function(index, d) {
        chartData.push(d.value);
    });
     Highcharts.chart('barchart', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Pengujian Statistic',
            style: { "fontSize": "0.9em", "margin": "2px" }
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        series: [{
            type: 'column',
            name: 'Pengujian',
            data: chartData
        }, {
            type: 'spline',
            name: 'Target',
            data: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60]
        }]
     });

     Highcharts.chart('piechart', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Pengujian Statistic',
            style: { "display": "none" },
            floating: true
        },
        series: [{
            name: 'Pengujian Statistic',
            data: [
                {
                    name: 'Pengujian',
                    y: <?=$current_repair?>,
                    selected: true
                },
                {
                    name: 'Target',
                    y: <?=$target_repair?>
                }
            ]
        }]
     });
    </script>
<?php
}
?>


<?php
function dashboard_manager() {
$lintas=new lintas;
$koneksi=$lintas->koneksi();
$bulan= ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];
$posisi = $lintas->userToGroup($_SESSION['username']);
$target_repair = 60;
$target_pengujian = 1200 ;
$i = 1;
/* User Pengujian */
$sql = mysqli_query($koneksi, "select username, nama_user from user where posisi='PengujianVsat' OR posisi='PengujianWireless'");
while($u = mysqli_fetch_assoc($sql)) {
    $user_pengujian[] = [
        'name' => $u['nama_user'],
        'username' => $u['username'],
    ];
}

/* User Repair */
$sql = mysqli_query($koneksi, "select username, nama_user from user where posisi='Repair'");
while($u = mysqli_fetch_assoc($sql)) {
    $user_repair[] = [
        'name' => $u['nama_user'],
        'username' => $u['username'],
    ];
}

foreach ($bulan as $value) {
    foreach ($user_pengujian as $key => $val) {
        /* Data Pengujian */
        $d = mysqli_query($koneksi, "select pengujian.* from pengujian INNER JOIN info ON pengujian.no_info = info.no_info WHERE MONTH(tgl_uji)={$i} AND penguji='{$val['username']}' GROUP BY no_regis");
        $data_pengujian[$key]['name'] = $val['name'];
        if ($i > date('m')) {
            $data_pengujian[$key]['data'][] = null;
        } else {
            $data_pengujian[$key]['data'][] = mysqli_num_rows($d);
        }
    }

    foreach ($user_repair as $key => $val) {
        /* Data Repair */
        $d = mysqli_query($koneksi, "select * from repair where MONTH(tanggal)={$i} AND id_user='{$val['username']}'");
        $data_repair[$key]['name'] = $val['name'];
        if ($i > date('m')) {
            $data_repair[$key]['data'][] = null;
        } else {
            $data_repair[$key]['data'][] = mysqli_num_rows($d);
        }
    }

    $i++;
}


$year_now = date('Y');
$target_r = 60 * 12;
$target_p = 1200 * 12;


/* Pie Pengujian */
$p = mysqli_query($koneksi, "select pengujian.* from pengujian INNER JOIN info ON pengujian.no_info = info.no_info WHERE YEAR(tgl_uji)={$year_now} GROUP BY no_regis");
$pre = mysqli_num_rows($p);
$current_pengujian = ($pre / $target_p) * 100;
$target_pengujian = 100 - $current_pengujian;

/* Pie Repair */
$p = mysqli_query($koneksi, "select * from repair where YEAR(tanggal)={$year_now}");
$pre = mysqli_num_rows($p);
$current_repair = ($pre / $target_r) * 100;
$target_repair = 100 - $current_repair;

?>
    <div class="col-md-12" id="dashboard">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default" style="height:330px;">
                    <div class="panel-heading"><i class="fa fa-smile-o"></i> Greetings </div>
                    <div class="panel-body">
                        <h3 style="margin: 5px 0">Welcome</h3>
                        <b><?=$_SESSION['name']?></b>
                        </br>
                        </br>
                        <h3 style="margin: 5px 0">Posisi</h3>
                        <b><?=$posisi?></b>
                        </br>
                        </br>
                        <h5><?= date('D, H:i:s') ?></h5>
                        <h3 style="margin:0"><?= date('d-M-Y') ?> </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default" style="height:330px;">
                    <div class="panel-heading"><i class="fa fa-pie-chart"></i> Target Pengujian </div>
                    <div class="panel-body">
                        <div id="piechart_pengujian" style="height:250px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default" style="height:330px;">
                    <div class="panel-heading"><i class="fa fa-pie-chart"></i> Target Repair </div>
                    <div class="panel-body">
                        <div id="piechart_repair" style="height:250px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="barchart_pengujian" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="barchart_repair" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../js/plugins/morris/raphael.min.js"></script>
    <script src="../js/plugins/morris/morris.min.js"></script>
    <script>

    var chartRepair = [];
    var target_repair = {
        name: 'target_repair',
        data: [60, 60, 60, 60, 60, 60, 60, 60, 60, 60, 60 ,60]
    };
    var chartPengujian = [];
    var target_pengujian = {
        name: 'target_pengujian',
        data: [1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200 ,1200]
    };
    chartPengujian.push(target_pengujian);
    chartRepair.push(target_repair);
    $.each(<?=json_encode($data_pengujian)?>, function(index, d) {
        chartPengujian.push(d);
    });
    $.each(<?=json_encode($data_repair)?>, function(index, d) {
        chartRepair.push(d);
    });
    console.log(chartRepair);
    console.log(chartPengujian);
     Highcharts.chart('barchart_pengujian', {
        title: {
            text: 'Statistik Pengujian',
            style: { "fontSize": "0.9em", "margin": "2px" }
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        series: chartPengujian
     });

     Highcharts.chart('barchart_repair', {
        title: {
            text: 'Statistik Repair',
            style: { "fontSize": "0.9em", "margin": "2px" }
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        series: chartRepair
     });

     Highcharts.chart('piechart_pengujian', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Statistik Pengujian',
            style: { "display": "none" },
            floating: true
        },
        series: [{
            name: 'Statistik Pengujian',
            data: [
                {
                    name: 'Pengujian',
                    y: <?=$current_pengujian?>,
                    selected: true
                },
                {
                    name: 'Target',
                    y: <?=$target_pengujian?>
                }
            ]
        }]
     });

     Highcharts.chart('piechart_repair', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Pengujian Statistic',
            style: { "display": "none" },
            floating: true
        },
        series: [{
            name: 'Statistik Repair',
            data: [
                {
                    name: 'Repair',
                    y: <?=$current_repair?>,
                    selected: true
                },
                {
                    name: 'Target',
                    y: <?=$target_repair?>
                }
            ]
        }]
     });
    </script>
<?php
}
?>


<?php
   function repair(){
     $lintas=new lintas;
     $koneksi=$lintas->koneksi();
     $a = mysqli_query($koneksi, "select * from draft");
     $res = [];
     while($row = mysqli_fetch_assoc($a)) {
       $res[] = $row;
     };
   ?>
     <div class="col-md-12" id="repair">
       <h3><i class="fa fa-file-text-o fa-fw"></i> Repair</h3>
       <hr>
       <div class="row">
         <div class="col-md-12">
           <div class="panel panel-default">
               <div class="panel-body">
                   <table class="table table-condensed">
                     <tr>
                       <th width="150">Engineer</th>
                       <td class="form-inline"><input class="form-control" placeholder="Engineer" type="text"></td>
                     </tr>
                     <tr>
                       <th>Appv.Data Entry</th>
                       <td class="form-inline"><input class="form-control" placeholder="Appv. Data Entry" type="text"></td>
                     </tr>
                     <tr>
                       <th>Clasification</th>
                       <td class="form-inline"><input class="form-control" placeholder="Clasification" type="text"></td>
                     </tr>
                   </table>
               </div>
           </div>
         </div>
       </div>
       <div class="row">
         <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Clasification </div>
                <div class="panel-body">
                    <table class="table table-bordered table-condensed">
                      <tr>
                        <th>1. Radiolink</th>
                        <td>SIAE,Nera</td>
                        <td>SIAE,Nera</td>
                        <th>6. BWA 5</th>
                        <td>Wibas</td>
                      </tr>
                      <tr>
                        <th>2. BWA 1</th>
                        <td>SASA,PSU SAS</td>
                        <td>SASA,PSU SAS</td>
                        <th>7. VSAT</th>
                        <td>Modem,HPA,LNB/LNA,Modulator</td>
                      </tr>
                      <tr>
                        <th>3. BWA 2</th>
                        <td>TSBU,TS RFU,BS BU</td>
                        <td>TSBU,TS RFU,BS BU</td>
                        <th>8. Networking/Switching</th>
                        <td>Cisco,TPLink</td>
                      </tr>
                      <tr>
                        <th>5. BWA 3</th>
                        <td>Hariff,Himax,Articonet</td>
                        <td>Hariff,Himax,Articonet</td>
                        <th>9.WAN Optimizer</th>
                        <td>Riverbed</td>
                      </tr>
                      <tr>
                        <th>5. BWA 4</th>
                        <td>Radwin</td>
                        <td>Radwin</td>
                        <th>10.UPS/Rectifier</th>
                        <td></td>
                      </tr>
                    </table>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-md-12">
              <form id="table-rpr">
                <input type="hidden" name="action" value="save_repair">
              <div class="table-responsive">
                <table class="table table-bordered">
                  <tr>
                      <th>No.</th>
                      <th>No Registrasi</th>
                      <th>Nama Perangkat</th>
                      <th>Tanggal</th>
                      <th>Analisa Kerusakan</th>
                      <th>Parts Terpakai</th>
                      <th>Ket</th>
                  </tr>
                  <tbody class="table-repair">
                    <?php for($i = 0; $i < 14; $i++):
                      $data = [
                        "no_draft" => '',
                        "noregis" => '',
                        "nama" => '',
                        "tanggal" => '',
                        "analisa" => '',
                        "parts" => '',
                        "ket" => '',
                        "iduser" => '',
                      ];
                      if (isset($res[$i])){
                        $data = $res[$i];
                        $data['tanggal'] = $lintas->tglindo($data['tanggal']);
                      }
                    ?>
                      <tr id="tr-rpr-<?=$i?>">
                          <input type="hidden" value="<?=$data['no_draft']?>" name="no_draft[]" >
                          <td><?=$i + 1?></td>
                          <td><input type="text" tr-rpr="<?=$i?>" value="<?=$data['noregis']?>" class="noregis-rpr form-control" name="noregis_rpr[]" id="noregis_rpr[]"></td>
                          <td><input type="text" value="<?=$data['nama']?>" class="nama-rpr form-control" name="nama_rpr[]" id="nama_rpr[]"></td>
                          <td><input style="max-width:80px;" value="<?=$data['tanggal']?>" class="form-control datepicker" class="form-control" name="tanggal_rpr[]"></td>
                          <td><input type="text" class="form-control" value="<?=$data['analisa']?>" name="analisa[]"></td>
                          <td><input type="text" class="form-control" value="<?=$data['parts']?>" name="parts[]"></td>
                          <td><input type="text" class="form-control" value="<?=$data['ket']?>" name="ket[]"></td>
                      </tr>
                    <?php endfor; ?>
                  </tbody>
                </form>
                </table>
             </div>
           </div>
         </div>
         <div class="row">
           <div class="col-md-12">
             <div id="buttonrpr">
               <button type="button" id="btn-sendrpr" class="btn btn-warning">Send</button>
               <button type="button" id="btn-draftrpr" class="btn btn-success">Save As Draft</button>

             </div>
           </div>
         </div>
       </div>
       <script>
        $(".datepicker").datepicker({
          dateFormat: 'dd-mm-yy'
        });
       </script>
   <?php
    }

    ?>
<?php
function riwayat_repair(){
  $lintas=new lintas;
  $koneksi=$lintas->koneksi();
?>
  <div class="col-md-12">
    <form id="riwayat_repair">
    <h3><i class="fa fa-clipboard fa-fw"></i> Riwayat Repair</h3>
    <hr />
    <div class="form-group form-inline">
      <label for="tanggal_riwayat">Search by Date:  </label>
        <input type="text" class="datepicker form-control" name="search" id="tanggal_riwayat">
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr>
                      <th>No.</th>
                      <th>No Registrasi</th>
                      <th>Tanggal</th>
                      <th>Analisa Kerusakan</th>
                      <th>Parts Terpakai</th>
                      <th>Repaired by</th>
                  </tr>
                </thead>
                <tbody id="tbody_riwayat_repair">
                    <?php
                    $q=mysqli_query($koneksi,"SELECT * FROM repair ");
                    $no=0;
        			while($d=mysqli_fetch_row($q)) {
                    $no++;
        				echo "<tr><td>$no</a></td><td>$d[1]</td><td>$d[3]</td><td>$d[5]</td><td>$d[6]</td>
                        <td>$d[8]</td>
                      </tr>";
        			}
        			?>
                </tbody>
              </table>
            </div>
        </div>
    </div>

    </form>
  </div>

<script>
 $(".datepicker").datepicker({
   dateFormat: 'dd-mm-yy'
 });
</script>
<?php
}
?>
<?php
   function kategori(){
     ?>
     <div class="col-md-12" id="kategori">
       <h3><i class="fa fa-fw fa-table"></i> Kategori Perangkat</h3>
       <hr />
       <div class="table-responsive">
         <div id="tambahkat">
           <form id="form_kat_nambah" role="form">
             <div id="kat_nambah_alert"></div>
             <div class="form-group" id="namakat">
            <label for="kategori">Tambah Kategori:</label>
            <input type="text" class="form-control" name="kategori">
          </div>
        </div>
          <div>
            <button type="button" id="buttonkat" class="btn btn-warning">Simpan</button>
          </form>
          </div>

          <!-- Modal Simpan-->
         <div class="modal fade" id="modal-simpan-sukses" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="opacity:0.9">
              <div class="vertical-alignment-helper">
                  <div class="modal-dialog modal-sm vertical-align-center">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                               <h4 class="modal-title" id="myModalLabel">Data berhasil disimpan</h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="id_kategori" >
                          </div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-success" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>

                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="modal fade" id="modal-hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="opacity:0.9">
               <div class="vertical-alignment-helper">
                   <div class="modal-dialog modal-sm vertical-align-center">
                       <div class="modal-content">
                           <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
                           </div>
                           <div class="modal-body">
                             <input type="hidden" name="id_kategori" >
                           </div>
                           <div class="modal-footer">
                               <button type="button" class="btn" data-dismiss="modal"><span class="fa fa-times fa-fw"></span> Tidak</button>
                               <a onclick="hapus_kategori()" href="#" class="btn btn-danger"><span class="fa fa-trash fa-fw"></span> Ya</a>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
            <!--End Modal-->
            <!-- Modal Kategori -->
            <div class="modal fade" id="modal-kategori-edit" tabindex="-1" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Kategori</h4>
                  </div>
                  <form>
                    <div class="modal-body">
                      <div class="form-group">
                        <label>Nama Kategori</label>
                        <input class="form-control" type="hidden" name="id_kategori">
                        <input class="form-control" name="nama_kategori">
                      </div>
                    </div>
                  </form>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="update_kategori()" class="btn btn-primary">Save changes</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <!-- Modal Kategori -->

          <br>
          <div class="panel panel-default">
          <div class="panel-body">
          <table class="table table-hover">
            <thead>
              <tr>
                 <th>No.</th>
                 <th>Nama Kategori</th>
                 <th></th>
              </tr>
           </thead>
           <tbody></tbody>
         </table>
        </div>
      </div>
    </div>
  </div>
      <?php
 }
       ?>
       <?php
       function parameter(){
         ?>
         <div class="col-md-12" id="parameter">
           <form id="par">
             <h3><i class="fa fa-fw fa-table"></i> Parameter Uji</h3>
             <hr />
            <div class="table-responsive">
              <div id="tambahpar">
                <form id="form_par_nambah" role="form">
                <div id="par_nambah_alert"></div>
                <div class="form-group" id="namapar">
                 <label for="parameter">Tambah Parameter:</label>
                 <input type="text" class="form-control" name="parameter">
               </div>
             </div>
               <div>
                 <button type="button" id="buttonpar" class="btn btn-warning">Simpan</button>
               </form>
               </div>

               <!-- Modal Simpan-->
              <div class="modal fade" id="modal-simpan-parameter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="opacity:0.9">
                   <div class="vertical-alignment-helper">
                       <div class="modal-dialog modal-sm vertical-align-center">
                           <div class="modal-content">
                               <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Data berhasil disimpan</h4>
                               </div>
                               <div class="modal-body">
                                 <input type="hidden" name="no_parameter" >
                               </div>
                               <div class="modal-footer">
                                   <button type="button" class="btn btn-success" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
                <!-- Modal Hapus-->
               <div class="modal fade" id="modal-hapus-parameter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="opacity:0.9">
                    <div class="vertical-alignment-helper">
                        <div class="modal-dialog modal-sm vertical-align-center">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                     <h4 class="modal-title" id="myModalLabel">Konfirmasi Hapus</h4>
                                </div>
                                <div class="modal-body">
                                  <input type="hidden" name="id_parameter" >
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn" data-dismiss="modal"><span class="fa fa-times fa-fw"></span> Tidak</button>
                                    <a onclick="hapus_parameter()" href="#" class="btn btn-danger"><span class="fa fa-trash fa-fw"></span> Ya</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <!--End Modal-->
                 <!-- Modal Kategori -->
                 <div class="modal fade" id="modal-parameter-edit" tabindex="-1" role="dialog">
                   <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                         <h4 class="modal-title">Edit Parameter</h4>
                       </div>
                       <form>
                         <div class="modal-body">
                           <div class="form-group">
                             <label>Nama Parameter</label>
                             <input class="form-control" type="hidden" name="id_parameter">
                             <input class="form-control" name="nama_parameter">
                           </div>
                         </div>
                       </form>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                         <button type="button" onclick="update_parameter()" class="btn btn-primary">Save changes</button>
                       </div>
                     </div><!-- /.modal-content -->
                   </div><!-- /.modal-dialog -->
                 </div><!-- /.modal -->
                 <!-- Modal Kategori -->
               <br>
               <div class="panel panel-default">
               <div class="panel-body">
              <table class="table table-hover">
                <thead>
                  <tr>
                     <th>No.</th>
                     <th>Parameter Uji</th>
                     <th>Hapus</th>
                  </tr>
               </thead>
               <tbody></tbody>
             </table>
           </form>
            </div>
          </div>
        </div>
      </div>


    <?php
          }
function detail(){
      $lintas=new lintas;
      $koneksi=$lintas->koneksi();
      ?>
      <div class="col-md-12" id="detail">
        <h3><i class="fa fa-fw fa-table"></i> Detail Kategori</h3>
        <hr />
        <div id="detailkat">
        <form id="form_detail_kat" role="form">
          <div class="form-group" id="pilihkat">
          <label for="pilihkat">Pilih Kategori:</label>
          <select class="form-control" name="pilihkat">
            <option value="">Pilih Kategori</option>
              <?php
              $q=mysqli_query($koneksi,"SELECT id_kat,kategori FROM kategori ORDER BY kategori ASC") or die("gagal");
              while($d=mysqli_fetch_row($q)) {
                echo "<option value=$d[0]>$d[1]</option>";
              }
              ?>
            </select>
          </div>
         </div>
          <div class="form-group" id="pilihpar">
          <label for="pilihpar">Pilih Parameter Uji:</label><br>
          <div id="pilihPar">
          <?php
            $q=mysqli_query($koneksi,"SELECT id_par,parameter FROM parameter ORDER BY parameter ASC");
            while($d=mysqli_fetch_row($q)) {
              $arr= array(0,1,2,3,4,5,6,7,8,9);
              echo "<div class='col-md-4'>
                      <select name=urutan[] data-id=$d[0]>";
              foreach ($arr as $value) {
                echo "<option value='$value-$d[0]'>$value</option>";
              }
              echo "</select> $d[1]<br></div>";
          }
          ?>
            </div>
         </div>
         <BR>
       <div>
         <button type="button" id="buttondetail" class="btn btn-warning">Simpan</button>
       </div>
       </form>
        </div>


 <?php
       }

 function manage_user(){
 	global $koneksi;
 ?>
 	<div class="col-md-12" id="manage_user">
    <h3><i class="fa fa-fw fa-users"></i> Management User </h3>
    <hr />
        <ul class="nav nav-tabs" style="margin-bottom:-22px">
            <li class="active"><a data-toggle="tab" href="#user_data">Data User</a></li>
            <li><a data-toggle="tab" href="#user_isi">Pengisian Data</a></li>
        </ul>
		<div class="tab-content">
			<div id="user_data" class="tab-pane fade in active">
				<?php
                if(isset($_POST['update']) || isset($_POST['hapus'])) {
                    user_data();
                } elseif (isset($_GET['p'])) {
                    $p=$lintas->dekrip($_GET['p']);

                    if($p=='edit') {
                        $user=$lintas->dekrip($_GET['i']);
                        user_edit($user);
                        user_data();
                    } else {
                        header('location:logout.php');
                    }
                } else {
                    user_data();
                }
                ?>
            </div>
            <div id="user_isi" class="tab-pane fade">
                <?php user_input()?>
            </div>
       </div>
    </div>
<?php
}

 function user_data() {
 	global $koneksi,$lintas;
 	?>
  <BR>
  <div class="panel panel-default">
  <div class="panel-body">
 <div class="table-responsive" id="datauser">
           <?php
 			if(isset($_POST['simpan'])) {
 				if(mysqli_affected_rows($koneksi)>0) {
 					echo "<div class='alert alert-success'>
 						  <a href=# class='close' data-dismiss='alert' aria-label='close'>&times;</a>
 						  Data berhasil dimasukkan...</div>";
 				}
 				else {
 					echo "<div class='alert alert-danger'>
 						  <a href=# class='close' data-dismiss='alert' aria-label='close'>&times;</a>
 						  <strong>Gagal masuk!</strong></div>";
 				}
 			}
 			elseif(isset($_POST['update'])) {
 				if(mysqli_affected_rows($koneksi)>0) {
 					echo "<div class='alert alert-success'>
 						  <a href=# class='close' data-dismiss='alert' aria-label='close'>&times;</a>
 						  Data berhasil diubah...</div>";
 				}
 			}
 			elseif(isset($_POST['hapus'])) {
 				if(mysqli_affected_rows($koneksi)>0) {
 					echo "<div class='alert alert-danger'>
 						  <a href=# class='close' data-dismiss='alert' aria-label='close'>&times;</a>
 						  Data berhasil dihapus...</div>";
 				}
 			}
 		   ?>

           <table class="table">
             <thead>
               <tr>
                 <th>Username</th><th>Nama</th><th>Posisi</th><th>Hapus User</th>
               </tr>
             </thead>
             <tbody>
             <?php
             $q=mysqli_query($koneksi,"SELECT username,nama_user,posisi,id_user FROM user ORDER BY username ASC");
 			while($d=mysqli_fetch_row($q)) {
        echo "<tr>
                <td><a href='#' onclick='editmanageuser(\"$d[0]\",\"$d[1]\",\"$d[2]\")'>$d[0]</a></td>
                <td>$d[1]</td>
                <td>$d[2]</td>
                <td><a href='#' onclick='konfirmasi_hapus_user(\"$d[0]\", \"$d[3]\")'><i class='fa fa-trash-o fa-fw'></i></a></td>
              </tr>";
 			}
 			?>
             </tbody>
           </table>
           </div>
          </div>
        </div>

	<div class="modal fade" id="modal-user-edit" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Edit User</h4>
            </div>
            <form>
              <div class="modal-body">
                <div class="form-group">
                  <label>Username:</label>
                  <input class="form-control" name="username" readonly value=""></input>
                  <label>Nama User:</label>
                  <input class="form-control" name="nama" readonly value=""></input>
                  <label>Posisi:</label>
                  <select name="posisi" class="form-control">
                    <option value="">Pilih Posisi</option>
                    <?php
                    $q=mysqli_query($koneksi,"SELECT posisi FROM user GROUP BY posisi ORDER BY posisi ASC");
                    while($d=mysqli_fetch_row($q)) {
                      echo "<option value=$d[0]>$d[0]</option>";
                    }
                     ?>
                </select>
                </div>
              </div>
            </form>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" name="update" onclick="update_manage_user()" class="btn btn-primary">Save changes</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- Modal Kategori -->

 <?php
 }

 function user_input() {
 	global $koneksi,$user,$pass,$nama,$posisi;
 ?>
    <BR>
    <div class="panel panel-default">
    <div class="panel-body">
    <form role="form" method="post" id="tambah_user">
     <div class="form-group">
       <label for="username">Username:</label>
       <input name="username" required type="text" class="form-control" placeholder="Masukkan username">
     </div>
     <div class="form-group">
       <label for="password">Password:</label>
       <input type="password" required class="form-control" name="password" placeholder="Masukkan password">
     </div>
     <div class="form-group">
       <label for="nama">Nama:</label>
       <input type="text" class="form-control" name="nama_user" required placeholder="Masukkan nama">
     </div>
     <div class="form-group">
       <label for="posisi">Posisi:</label>
       <select name="posisi" class="form-control">
         <option value="">Pilih Posisi</option>
         <?php
         $q=mysqli_query($koneksi,"SELECT posisi FROM user GROUP BY posisi ORDER BY posisi ASC");
         while($d=mysqli_fetch_row($q)) {
           echo "<option value=$d[0]>$d[0]</option>";
         }
          ?>
     </select>
     </div>
     <button type="button" name="simpan" onclick="tambah_user()" class="btn btn-warning">Save</button>
   </form>
 </div>
 </div>
 <?php
 }

 function rekap_repair(){
 ?>
  <div class="col-md-12">
   <form id="rekap_repair">
   <h3><i class="fa fa-fw fa-folder-open-o"></i> Rekap Repair </h3>
   <hr>
   	<div class="panel panel-default">
     <div class="panel-body">
     	<div class="form-group">
        <label for="tgl_mulai">Dari Tanggal:</label>
          <input type="text" class="datepicker form-control" name="tgl_mulai">
      </div>
      <div class="form-group">
        <label for="tgl_akhir">Sampai Tanggal:</label>
          <input type="text" class="datepicker form-control" name="tgl_akhir">
      </div>
      <div class="form-group">
        	<button class="btn btn-primary" type="button" id="btn-download-rekap"> Download File Rekap </button>
      </div>
     </div>
    </div>
   </form>
 </div>
   <script>
	$(".datepicker").datepicker({
	  dateFormat: 'dd-mm-yy'
	});
   </script>
 <?php
}
 ?>

 <?php
 function rekap_pengujian(){
   global $koneksi;
 ?>
 <div class="col-md-12">
 <form  id="rekap_pengujian">
 <h3><i class="fa fa-fw fa-folder-open-o"></i> Rekap Pengujian </h3>
 <hr>
 <div class="panel panel-default">
  <div class="panel-body">
    <div class="form-group">
      <label for="kategori">Kategori Perangkat:</label>
      <select name="kategori" class="form-control">
        <option value="">Pilih Kategori</option>
        <?php
        $q2=mysqli_query($koneksi,"SELECT id_kat,kategori FROM kategori ORDER BY kategori ASC");
        while($d2=mysqli_fetch_row($q2)) {
          echo "<option value=$d2[0]>$d2[1]</option>";
        }
         ?>
      </select>
    </div>
    <div class="form-group">
      <label for="tgl_mulai">Dari Tanggal:</label>
       <input type="text" class="datepicker form-control" name="tgl_mulai">
      </div>
      <div class="form-group">
       <label for="tgl_akhir">Sampai Tanggal:</label>
         <input type="text" class="datepicker form-control" name="tgl_akhir">
      </div>
      <div class="form-group">
        <button class="btn btn-primary" type="button" id="btn-download-rekap-pengujian"> Download File Rekap </button>
      </div>
 </form>
 </div>
 </div>
 </div>
 <script>
 $(".datepicker").datepicker({
  dateFormat: 'dd-mm-yy'
 });
 </script>
 <?php
 }
 ?>

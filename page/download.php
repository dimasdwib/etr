<?php

include 'func.php';
$kategori = isset($_GET['kategori']) ? $_GET['kategori'] : false;
$lintas = new lintas();
$tmta = $lintas->tglindo($_GET['tgl_mulai']).'_sd_'.$lintas->tglindo($_GET['tgl_akhir']);
$tm = explode('-', $_GET['tgl_mulai']);
$ta = explode('-', $_GET['tgl_akhir']);

// kategori = false = repair
if ($kategori == false) {

	// Nama file
	$filename = 'rekap_repair'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * FROM repair WHERE tanggal_kirim between '{$start}' and '{$end}'");
	$i = 1;

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query)){
	    if (!isset($data_pdf[$r['tanggal_kirim']])) {
	        $data_pdf[$r['tanggal_kirim']] = [];
	        $data_pdf[$r['tanggal_kirim']][] = [1, $r['no_regis'], $r['nama'], $r['tanggal'], $r['analisa'], $r['parts'] , $r['keterangan']];
	    } else {
	        $data_pdf[$r['tanggal_kirim']][] = [count($data_pdf[$r['tanggal_kirim']]) + 1, $r['no_regis'], $r['nama'], $r['tanggal'], $r['analisa'], $r['parts'] , $r['keterangan']];
	    }
	    $i++;
	};

	//var_dump($data_pdf);
	//die();

	$lintas->repair_pdf($filename, $data_pdf);
	exit();

} else {

	if ($kategori == 1) {
	$filename = 'rekap_net'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	$info = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[1]) ? $param[1] : '',
				isset($param[11]) ? $param[11] : '',
				isset($param[10]) ? $param[10] : '',
				isset($param[12]) ? $param[12] : '',
				isset($param[9]) ? $param[9] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	// var_dump($info);
	// die();
	$lintas->netuji_pdf($filename, $data_pdf, $info);
	exit();
	} elseif ($kategori ==2) {
	$filename = 'rekap_link'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[2]) ? $param[2] : '',
				isset($param[9]) ? $param[9] : '',
				isset($param[10]) ? $param[10] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->linkuji_pdf($filename, $data_pdf, $info);
	exit();
	} elseif ($kategori == 3) {
	$filename = 'rekap_vms'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[2]) ? $param[2] : '',
				isset($param[3]) ? $param[3] : '',
				isset($param[4]) ? $param[4] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->vmsuji_pdf($filename, $data_pdf, $info);
	exit();
}elseif ($kategori == 4) {
	$filename = 'rekap_bwa5'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[13]) ? $param[13] : '',
				isset($param[1]) ? $param[1] : '',
				isset($param[4]) ? $param[4] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->bwa5uji_pdf($filename, $data_pdf, $info);
	exit();
} elseif ($kategori == 5) {
	$filename = 'rekap_bwa1'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[10]) ? $param[10] : '',
				isset($param[16]) ? $param[16] : '',
				isset($param[14]) ? $param[14] : '',
				isset($param[15]) ? $param[15] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->bwa1uji_pdf($filename, $data_pdf, $info);
	exit();
} elseif ($kategori == 6) {
	$filename = 'rekap_bwa1psu'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[10]) ? $param[10] : '',
				isset($param[17]) ? $param[17] : '',
				isset($param[18]) ? $param[18] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->bwa1psuuji_pdf($filename, $data_pdf, $info);
	exit();
} elseif ($kategori == 7) {
	$filename = 'rekap_bwa2'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[10]) ? $param[10] : '',
				isset($param[16]) ? $param[16] : '',
				isset($param[19]) ? $param[19] : '',
				isset($param[20]) ? $param[20] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->bwa2uji_pdf($filename, $data_pdf, $info);
	exit();
} elseif ($kategori == 8) {
	$filename = 'rekap_bwa3articonet'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[10]) ? $param[10] : '',
				isset($param[21]) ? $param[21] : '',
				isset($param[22]) ? $param[22] : '',
				isset($param[23]) ? $param[23] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->bwa3articonetuji_pdf($filename, $data_pdf, $info);
	exit();
} elseif ($kategori == 9) {
	$filename = 'rekap_bwa3hariff'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[10]) ? $param[10] : '',
				isset($param[21]) ? $param[21] : '',
				isset($param[24]) ? $param[24] : '',
				isset($param[25]) ? $param[25] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->bwa3hariffuji_pdf($filename, $data_pdf, $info);
	exit();
} elseif ($kategori == 10) {
	$filename = 'rekap_router'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[1]) ? $param[1] : '',
				isset($param[10]) ? $param[10] : '',
				isset($param[26]) ? $param[26] : '',
				isset($param[27]) ? $param[27] : '',
				isset($param[21]) ? $param[21] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->routeruji_pdf($filename, $data_pdf, $info);
	exit();
} elseif ($kategori == 11) {
	$filename = 'rekap_radiolink'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[10]) ? $param[10] : '',
				isset($param[16]) ? $param[16] : '',
				isset($param[19]) ? $param[19] : '',
				isset($param[15]) ? $param[15] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->radiolinkuji_pdf($filename, $data_pdf, $info);
	exit();

} elseif ($kategori == 12) {
	$filename = 'rekap_wireline'.$tmta.'.pdf';

	$start = $tm[2].'-'.$tm[1].'-'.$tm[0];
	$end = $ta[2].'-'.$ta[1].'-'.$ta[0];

	$lintas = new lintas();
	$koneksi = $lintas->koneksi();
	$query = mysqli_query($koneksi, "SELECT * from info where tgl_uji between '{$start}' and '{$end}'");
	$query2 = mysqli_query($koneksi, "SELECT pengujian.no_regis, tgl_uji, penguji, appv, asal, batch_in, batch_out, nama, status_awal from info JOIN pengujian ON info.no_info=pengujian.no_info where info.tgl_uji between '{$start}' and '{$end}' group by pengujian.no_regis");

	$data_pdf = [];
	while($r = mysqli_fetch_assoc($query2)) {
		//if(
			//$data_pdf[$r['no_regis']] = [];
			//$data_pdf[$r['no_regis']][] = [1,$r['no_regis']];

			$info[$r['tgl_uji']] = [
				'penguji' => $r['penguji'],
				'appv' => $r['appv'],
				'asal' => $r['asal'],
				'batch_in' => $r['batch_in'],
				'batch_out' => $r['batch_out'],
				'status_awal' => $r['status_awal'],
				'nama' => $r['nama'],
			];

			$param = [];
			$q_parameter = mysqli_query($koneksi, "select id_par, hasil_uji from pengujian where no_regis='{$r['no_regis']}' AND status = 'uji'");
			while($p = mysqli_fetch_assoc($q_parameter)) {
				$param[$p['id_par']] = $p['hasil_uji'];
			}

			$data_pdf[$r['tgl_uji']][] = [
				$r['no_regis'],
				isset($param[10]) ? $param[10] : '',
				isset($param[26]) ? $param[26] : '',
				isset($param[15]) ? $param[15] : '',
				isset($param[5]) ? $param[5] : '',
				isset($param[7]) ? $param[7] : '',
				isset($param[8]) ? $param[8] : '',
			];

		}

	//var_dump($data_pdf);
	//die();
	$lintas->wirelineuji_pdf($filename, $data_pdf, $info);
	exit();

	}

}

<?php
session_start();
if(empty($_SESSION['username'])) {
  //header('location:../index.php');
}
// konfigurasi database
include "func.php";
include "view.php";
$lintas=new lintas;
$koneksi=$lintas->koneksi();
$posisi=$lintas->userToGroup($_SESSION['username']);
// echo "CEK SESI: ".$_SESSION['username'];
//session_start();
//if(empty($_SESSION['username'])) {
  //header('location:../index.php');
//}

function activepage($str) {
  $page = $_GET['page'] ? $_GET['page'] : 'dashboard';
  if ($str == $page) {
    return 'active';
  }
}

if(isset($_POST['simpan'])) {
	$user=$_POST['username'];	$pass=md5($_POST['password']);
	$posisi=$_POST['posisi']; $nama=$_POST['nama_user'];

	mysqli_query($koneksi,"INSERT INTO user (username,password,nama_user,posisi) VALUES ('$user','$pass','$nama','$posisi')");
}

/*elseif(isset($_POST['hapus'])) {
	$user=$_POST['username'];
	mysqli_query($koneksi,"DELETE FROM user WHERE username='$user'");
}

elseif(isset($_POST['update'])) {
  $user=$_POST['username'];	$pass=md5($_POST['password']);
	$posisi=$_POST['posisi']; $nama=$_POST['nama_user'];

	if(empty($pass)) mysqli_query($koneksi,"UPDATE user SET username='$user',nama_user='$nama',posisi='$posisi' WHERE username='$user'");
	else mysqli_query($koneksi,"UPDATE user SET password='$pass',nama_user='$nama',posisi='$posisi' WHERE username='$user'");
}*/

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ET&R Lintasarta Serpong</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="../css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/batch.js"></script>
    <script src="../js/highcharts.js"></script>
    <script src="../js/highcharts-data.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <script src="../jquery-ui/jquery-ui.min.js"></script>

    <!-- Function -->
    <script src="../js/function.js"></script>
<style>
#dashboard {
    margin-top: 15px;
}
</style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse top-nav-1 navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">ET&R <small> Lintas Arta Serpong</small></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user fa-fw"></i> <span class="display-name"><?php echo $lintas->userToNama($_SESSION['username'])?></span> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="ganti_pass"><i class="fa fa-fw fa-gear"></i> Edit Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse" id="navigasi">
                <ul class="nav navbar-nav side-nav">
                    <li style="border-bottom: 1px solid #171833; width: 100%;">
                        <div style="padding: 7px 9px;border-radius: 30px;width:40px; height:40px; float:left; background-color:#041225; margin: 15px 10px 15px 15px;">
                            <i style="color:rgb(219, 219, 219) !important" class="fa fa-user fa-2x"></i>
                        </div>
                        <div style="display: block; color: #fff; padding: 15px;">
                            <b class="display-name"><?=$_SESSION['name']?></b>
                            <br>
                            <small><?=$_SESSION['posisi']?></small>
                        </div>
                    </li>
            <?php
                    if($posisi=='Admin') {
                    ?>
                  <li><a href="batch"><i class="fa fa-barcode fa-fw"></i> Batch In</a></li>
				          <li><a href="manage_user"><i class="fa fa-fw fa-users"></i> Management User</a></li>
                  <li><a href="riwayat_pengujian"><i class="fa fa-fw fa-file-text-o"></i> Riwayat Pengujian</a></li>
                  <li><a href="rekap_pengujian"><i class="fa fa-fw fa-folder-open-o"></i> Rekap Pengujian</a></li>
                  <li><a href="riwayat_repair"><i class="fa fa-fw fa-file-text-o"></i> Riwayat Repair</a></li>
                  <li><a href="rekap_repair"><i class="fa fa-fw fa-folder-open-o"></i> Rekap Repair</a></li>
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-th-large fa-fw"></i> Data Master<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                          <li>
                              <a href="kategori"><i class="fa fa-fw fa-table"></i> Kategori</a>
                          </li>
                          <li>
                              <a href="parameter"><i class="fa fa-fw fa-table"></i> Parameter Uji </a></li>
                          </li>
                          <li>
                              <a href="detail"><i class="fa fa-fw fa-table"></i> Detail Kategori </a></li>
                         </li>
                        </ul>
                    </li>
          <?php
                  }
          elseif($posisi=='PengujianVsat' || $posisi=='PengujianWireless'){
            echo"<li><a href=dashboard><i class=\"fa fa-fw fa-dashboard\"></i> Dashboard</a></li>";
            echo "<li><a href=pengujian><i class=\"fa fa-fw fa-file-text-o\"></i> Pengujian</a></li>";
            echo "<li><a href=riwayat_pengujian><i class=\"fa fa-fw fa-file-text-o\"></i> Riwayat Pengujian</a></li>";
            echo "<li><a href=rekap_pengujian><i class=\"fa fa-fw fa-folder-open-o\"></i> Rekap Pengujian</a></li>";
          }
          elseif($posisi=='Repair'){
            echo"<li><a href=dashboard><i class=\"fa fa-fw fa-dashboard\"></i> Dashboard</a></li>";
            echo "<li><a href=repair><i class=\"fa fa-fw fa-file-text-o\"></i> Repair</a></li>";
            echo "<li><a href=riwayat_repair><i class=\"fa fa-fw fa-clipboard\"></i> Riwayat Repair</a></li>";
            echo "<li><a href=rekap_repair><i class=\"fa fa-fw fa-folder-open-o\"></i> Rekap Repair</a></li>";
          }
          elseif($posisi=='SeniorEngineer'){
            echo"<li><a href=dashboard><i class=\"fa fa-fw fa-dashboard\"></i> Dashboard</a></li>";
            echo "<li><a href=riwayat_pengujian><i class=\"fa fa-fw fa-clipboard\"></i> Riwayat Pengujian</a></li>";
            echo "<li><a href=rekap_pengujian><i class=\"fa fa-fw fa-clipboard\"></i> Rekap Pengujian</a></li>";
            echo "<li><a href=riwayat_repair><i class=\"fa fa-fw fa-clipboard\"></i> Riwayat Repair</a></li>";
            echo "<li><a href=rekap_repair><i class=\"fa fa-fw fa-clipboard\"></i> Rekap Repair</a></li>";
          }
          elseif($posisi=='JuniorManager'){
            echo"<li><a href=dashboard><i class=\"fa fa-fw fa-dashboard\"></i> Dashboard</a></li>";
            echo "<li><a href=riwayat_pengujian><i class=\"fa fa-fw fa-clipboard\"></i> Riwayat Pengujian</a></li>";
            echo "<li><a href=riwayat_repair><i class=\"fa fa-fw fa-clipboard\"></i> Riwayat Repair</a></li>";
          }
          else // header("location:logout.php");
                    ?>

				</ul>
			</div>
            <!-- /.navbar-collapse -->
        </nav>

    <div id="page-wrapper">
        <div class="row">

          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
               <div class="vertical-alignment-helper">
                   <div class="modal-dialog vertical-align-center">
                       <div class="modal-content">
                           <div class="modal-header">
                               <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                               <h4 class="modal-title" id="myModalLabel"></h4>
                           </div>
                           <div class="modal-body">
                             <input type="hidden" name="no_parameter" >
                           </div>
                           <div class="modal-footer">
                               <button type="button" class="btn" data-dismiss="modal"><span class="fa fa-times fa-fw"></span> Close</button>
                           </div>
                       </div>
                   </div>
               </div>
           </div>

            <?php //include 'page/main.php';
          //  echo "CEK: ".$posisi;
              rekap_repair();
              dashboard();
              batch();
              pengujian();
              getinfo();
              riwayat_pengujian();
              rekap_pengujian();
              repair();
              riwayat_repair();
              kategori();
              parameter();
              detail();
              manage_user();
            ?>
        </div>
    </div>

    <!-- /#wrapper -->

    <!-- jQuery -->

     <!-- Modal -->
  <div class="modal fade" id="modal-warning-batch" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Warning!</h4>
        </div>
        <div class="modal-body">
          <p><i class="fa fa-fw fa-exclamation-circle"></i> No registrasi sudah ada</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <div class="modal fade" id="modal-ganti-pass" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="panel-body">
          <form id="form_password_change" role="form">
              <h4>Change Your Profile</h4>
              <br />
              <div class="form-group">
                <label>Posisi :</label>
                <input type="text" class="form-control" name="posisi" readonly value="<?=$posisi?>" ></input>
              </div>
              <div class="form-group">
                <label>Username :</label>
                <input type="text" class="form-control" name="username" readonly value="<?= $_SESSION['username']; ?>" ></input>
              </div>
              <div class="form-group">
                <label>Your Name:</label>
                <input type="text" class="form-control" name="nama_user" value="<?= $_SESSION['name']; ?>" ></input>
              </div>
              <small class="text-info"><i class="fa fa-info-circle fa-fw"></i> Kosongkan jika tidak ingin mengganti password</small>
              <br>
              <div class="form-group">
                <label>Old Password</label>
                <input type="password" min="6" placeholder="Old password" class="form-control" name="old_password" id="old_password">
                <span class="text-danger" id="old_password_error"></span>
              </div>
              <div class="form-group">
                <label>New Password</label>
                <input type="password" min="6" placeholder="New password" class="form-control" name="new_password" id="new_password">
                <span class="text-danger" id="new_password_error"></span>
              </div>
              <div class="form-group">
                <label>Confirm New Password</label>
                <input type="password" min="6" placeholder="Confirm password" class="form-control"  name="con_password"  id="con_newpassword" />
                <span class="text-danger" id="con_password_error"></span>
              </div>
              <br>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" name="password_change" onclick="update_profile('<?= $_SESSION['username'] ?>')" class="btn btn-primary" value="Change Password">Save changes</button>
          </form>
          <!--display success/error message-->
          <div id="message"></div>
        </div>
      </div>
    </div>
  </div>

</div>
<script>
$(document).ready(function() {

  var posisi = '<?= $posisi ?>';
  if (posisi == 'Admin') {
    $("#navigasi a[href='batch']").click();
  }

  $("a[href='ganti_pass']").on('click', function(e) {
    e.preventDefault();
    $("#modal-ganti-pass").modal('toggle');
  });

  $("#old_password").on('keyup', function() {
      $("#old_password_error").html("");
  });
  $("#new_password").on('keyup', function() {
      $("#new_password_error").html("");
  });
  $("#con_newpassword").on('keyup', function() {
      $("#con_password_error").html("");
  });
});
</script>

</body>

</html>

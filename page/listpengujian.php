<div class="col-md-12">
  <h1>Pengujian</h1>
  <hr>
  <div class="table-responsive">
    <div id="tbuji">
      <table class="table table-hover">
        <thead>
            <tr>
                <th>No.</th>
                <th>Tanggal</th>
                <th>No Registrasi</th>
                <th>Nama</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="datauji">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><a href="?page=detailpengujian"><i class="fa fa-cog" aria-hidden="true"></i></a></td>
            </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
  console.log('pengujian');
  get_pengujian();
});
function get_pengujian() {
	$.ajax({
			type: "GET",
			url: "pengujian.php",
			data: 'action=get_pengujian',
			success: function(data){
				var row;
				$.each(JSON.parse(data), function( index, value ) {
				  row += '<tr><td>'+value.id_batch+'</td><td>'+value.tanggal+'</td><td>'+value.noregis+'</td><td>'+value.nama_perangkat+'</td><td><a href="?page=detailpengujian&cat='+value.kategori+'"><i class="fa fa-cog" aria-hidden="true"></i></a></td><tr>';
				});
				$("#datauji").html(row);
			}
	});
}
</script>

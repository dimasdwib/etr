<?php
date_default_timezone_set("Asia/Jakarta");
include 'fpdf/fpdf.php';

class lintas {
  function koneksi(){
    $koneksi = mysqli_connect('localhost','root','','lintasarta');
    // $koneksi = mysqli_connect('localhost','lintasuser','lintasarta','lintasarta');
    return $koneksi;
  }
  function tglindo($tgl_YMD) {
    $e=explode("-",$tgl_YMD);
    $tglindo="$e[2]-$e[1]-$e[0]";
    return $tglindo;
  }

  function userToNama($username) {
        global $koneksi;
            $q=mysqli_query($koneksi,"SELECT nama_user FROM user WHERE username=\"$username\"");
            $d=mysqli_fetch_row($q);
            $nama_user=$d[0];
          return $nama_user;
    }
  function userToGroup($username) {
    global $koneksi;
    $q=mysqli_query($koneksi,"SELECT posisi FROM user WHERE username='$username'");
        $d=mysqli_fetch_row($q);
        return $d[0];
  }
  function enkrip($text) {
        $en=base64_encode($text);
        return $en;
    }

    function dekrip($text) {
        $dek=base64_decode($text);
        return $dek;
    }
function idparToParameter($id_par) {
        global $koneksi;
            $q=mysqli_query($koneksi,"SELECT parameter FROM parameter WHERE id_par=\"$id_par\"");
            $d=mysqli_fetch_row($q);
            $parameter=$d[0];
          return $parameter;
    }

function vcell($c_width,$c_height,$x_axis,$text, $obj) {
    $w_w=$c_height/3;
    $w_w_1=$w_w+2;
    $w_w1=$w_w+$w_w+$w_w+3;
    $len=strlen($text);// check the length of the cell and splits the text into 7 character each and saves in a array
    $txt_width = $c_width / 2;
    if($len > $txt_width) {
      $w_text=str_split($text, $txt_width);
      $obj->SetX($x_axis);
      $obj->Cell($c_width,$w_w_1,$w_text[0],'','','');
      $obj->SetX($x_axis);
      $obj->Cell($c_width,$w_w1,$w_text[1],'','','');
      $obj->SetX($x_axis);
      $obj->Cell($c_width,$c_height,'','LTRB',0,'L',0);
    } else {
      $obj->SetX($x_axis);
      $obj->Cell($c_width,$c_height,$text,'LTRB',0,'L',0);
    }
  }

  function repair_pdf($filename, $data_pdf) {
    $header = ['No', 'No Registrasi', 'Nama Perangkat', 'Tanggal', 'Analisa kerusakan', 'Parts terpakai', 'Ket'];
    $pdf = new FPDF('L', 'mm', 'A4');
    $lintas = new lintas();

    // Looping setiap file berdasarkan tanggal kirim
    foreach ($data_pdf as $data) :

        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'LEMBAR KEGIATAN REPAIR','C');

        $pdf->Ln(17);

        $pdf->SetFont('Arial','',10);

        $pdf->Cell(60,7, 'Engineer', 1);
        $pdf->Cell(0,7, '', 1);
        $pdf->Ln();
        $pdf->Cell(60,7, 'Appv.Data Entry', 1);
        $pdf->Cell(0,7, '', 1);
        $pdf->Ln();
        $pdf->Cell(60,7, 'Clasification', 1);
        $pdf->Cell(0,7, '', 1);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->Cell(50,7, '1. Radiolink');
        $pdf->Cell(50,7, 'SIAE,Nera');
        $pdf->Cell(50,7, 'SIAE,Nera');
        $pdf->Cell(50,7, '6. BWA 5');
        $pdf->Cell(50,7, 'Wibas');
        $pdf->Ln();
        $pdf->Cell(50,7, '2. BWA 1');
        $pdf->Cell(50,7, 'SASA,PSU SAS');
        $pdf->Cell(50,7, 'SASA,PSU SAS');
        $pdf->Cell(50,7, '7. VSAT');
        $pdf->Cell(50,7, 'Modem,HPA,LNB/LNA,Modulator');
        $pdf->Ln();
        $pdf->Cell(50,7, '3. BWA 2');
        $pdf->Cell(50,7, 'TSBU,TS RFU,BS,BU');
        $pdf->Cell(50,7, 'TSBU,TS RFU,BS,BU');
        $pdf->Cell(50,7, '8. Networking/Switching');
        $pdf->Cell(50,7, 'Cisco,TPLink');
        $pdf->Ln();
        $pdf->Cell(50,7, '4. BWA 3');
        $pdf->Cell(50,7, 'Hariff,Himax,Articonet');
        $pdf->Cell(50,7, 'Hariff,Himax,Articonet');
        $pdf->Cell(50,7, '9. WAN Optimizer');
        $pdf->Cell(50,7, 'Riverbed');
        $pdf->Ln();
        $pdf->Cell(50,7, '5. BWA 4');
        $pdf->Cell(50,7, 'Radwin');
        $pdf->Cell(50,7, 'Radwin');
        $pdf->Cell(50,7, '10. UPS/Rectifier');
        $pdf->Ln();
        $pdf->Ln();


        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i == 1) {
            $pdf->Cell(10,7, $value, 1);
          } else if ($i == 3) {
             $pdf->Cell(70,7, $value, 1);
          } else {
            $pdf->Cell(40,7, $value, 1);
          }
          $i++;
        }
        $pdf->Ln();

        foreach ($data as $row) {
          $i = 1;
          foreach ($row as $col) {
            if ($i == 1) {
              // $pdf->Cell(10,7, $col, 1);
              $x_axis = $pdf->getx();
              $c_width= 10;// cell width
              $c_height= 7;// cell height
              $this->vcell($c_width,$c_height,$x_axis, $col, $pdf);
            } elseif ($i == 3) { // nama perangkat
              // $pdf->Cell(70,7, $col, 1);
              $x_axis = $pdf->getx();
              $c_width= 70;// cell width
              $c_height= 7;// cell height
              $this->vcell($c_width,$c_height,$x_axis, $col, $pdf);
            } elseif ($i == 4) { // tanggal
              // $pdf->MultiCell(40,7, $lintas->tglindo($col), 1);
              $x_axis = $pdf->getx();
              $c_width= 40;// cell width
              $c_height= 7;// cell height
              $this->vcell($c_width,$c_height,$x_axis, $lintas->tglindo($col), $pdf);
            } else {
              // $pdf->MultiCell(40,7, $col, 1);
              $x_axis = $pdf->getx();
              $c_width= 40;// cell width
              $c_height= 7;// cell height
              $this->vcell($c_width,$c_height,$x_axis, $col, $pdf);
            }
            $i++;
          }
          $pdf->Ln();
        }

    endforeach;

    $pdf->Output($filename, 'D');
  }
  function set_info($info, $tgl, $pdf, $lintas) {
    $pdf->Cell(40,7, 'Asal Perangkat', 1);
    $pdf->Cell(40,7, $info[$tgl]['asal'], 1);
    $pdf->Cell(40,7, 'Penguji', 1);
    $pdf->Cell(50,7, $info[$tgl]['penguji'], 1);
    $pdf->Cell(30,7, 'Batch In', 1);
    $pdf->Cell(40,7, $info[$tgl]['batch_in'], 1);
    $pdf->Ln();
    $pdf->Cell(40,7, 'Tanggal Uji', 1);
    $pdf->Cell(40,7, $lintas->tglindo($tgl), 1);
    $pdf->Cell(40,7, 'Appv.Data Entry', 1);
    $pdf->Cell(50,7, $info[$tgl]['appv'], 1);
    $pdf->Cell(30,7, 'Batch Out', 1);
    $pdf->Cell(40,7, $lintas->tglindo($info[$tgl]['batch_out']), 1);
    $pdf->Ln();
    $pdf->Cell(40,7, 'Status Awal', 1);
    $pdf->Cell(40,7, $info[$tgl]['status_awal'], 1);
    $pdf->Cell(90,7, 'Nama Perangkat', 'LRTB',0,'C');
    $pdf->Cell(70,7, $info[$tgl]['nama'], 1);
  }
 function netuji_pdf($filename, $data_pdf, $info) {
    $header = ['No', 'No Registrasi', 'Fisik', 'Center LO', 'Power', 'C/N', 'Gain','Start-End','Result','KK'];
    $pdf = new FPDF('L', 'mm', 'A4');
    $lintas = new lintas();
    // var_dump($data_pdf);
    // die();

    // Looping setiap file berdasarkan tanggal kirim
    foreach ($data_pdf as $tgl => $data) :

        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(70);
        $pdf->Cell(40,10,'VSAT NET (BUC,LNB,LNA,Feed Horn,RFU,RF Tigris,RF EXCB)');

        $pdf->Ln(17);

        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(30,7, 'Parameter Uji', 1);
        $pdf->Cell(30,7, 'Rules', 1);
        $pdf->Cell(90,7, 'Kategori Kerusakan (KK)', 1);
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'Tidak Cacat/Mulus', 1);
        $pdf->Cell(30,7, '5.Center LO(SG)', 1);
        $pdf->Cell(30,7, '1.110Mhz+/-1%', 1);
        $pdf->Cell(45,7, '1.Tx Problem', 0);
        $pdf->Cell(45,7, '5.Connector Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '2.Gain BUC 2W (SW)', 1);
        $pdf->Cell(45,7, '45dB @ Tx-modem -40dBm', 1);
        $pdf->Cell(60,7, 'SW (software Power Meter)', 1);
        $pdf->Cell(45,7, '2.Rx Problem', 0);
        $pdf->Cell(45,7, '6.Fisik Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '3.Power BUC 2W (SW)', 1);
        $pdf->Cell(45,7, '33dB @ Tx-modem -22dBm', 1);
        $pdf->Cell(60,7, 'SA (Spectrum Analyzer)', 1);
        $pdf->Cell(45,7, '3.Interface Problem', 0);
        $pdf->Cell(45,7, '7.Software Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '4.Carrier to Noise (SA)', 1);
        $pdf->Cell(45,7, '50dB @ Tx-modem -22dBm', 1);
        $pdf->Cell(60,7, 'SG (Spectrum Analyzer)', 1);
        $pdf->Cell(45,7, '4.Power Problem', 'B');
        $pdf->Cell(45,7, '8.Other', 'RB');
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==6){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==8){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==9){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          } elseif ($i==10){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }
          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(20,7, $col, 1);
            } elseif ($i==6){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==7){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==8){
                $pdf->Cell(40,7, $col, 1);
            } elseif ($i==9){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }


    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');
  }

  function vmsuji_pdf($filename,$data_pdf,$info) {
    $header = ['No','No Registrasi','LED Indicator','SQF','Ping','Start-End','Result','KK'];
    $pdf = new FPDF('L', 'mm', 'A4');
    $lintas = new lintas();

    foreach ($data_pdf as $tgl => $data) :

        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(100);
        $pdf->Cell(40,10,'VSAT VMS (iDirect X1,X3,HX,HT)');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(30,7, 'Parameter Uji', 1);
        $pdf->Cell(120,7, 'Rules','LRTB',0,'C');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'Tidak Cacat/Mulus', 1);
        $pdf->Cell(30,7, '3.SQF', 1);
        $pdf->Cell(60,7, '< 45 (HX,HT)','RTB', 0,'C');
        $pdf->Cell(60,7, '< 70 (iDirect,X1,X3)', 'LRTB',0,'C');
        $pdf->Ln();

        $pdf->Cell(45,28, '2.LED Indicator', 1);
        $pdf->Cell(45,7, '', 1);
        $pdf->Cell(30,7, '4.Ping', 1);
        $pdf->Cell(60,7, 'not RTO', 'RTB',0,'C');
        $pdf->Cell(60,7, 'not RTO', 'LRTB',0,'C');
        $pdf->Ln();

        $pdf->Cell(45,7,'','RL',0);
        $pdf->Cell(22.5,7,'LAN',1);
        $pdf->Cell(22.5,7,'ON',1);
        $pdf->Cell(15,7,'System',1);
        $pdf->Cell(15,7,'ON',1);
        $pdf->Cell(120,7, 'Kategori Kerusakan (KK)','LRTB',0,'C');
        $pdf->Ln();

        $pdf->Cell(45,7,'','RL',0);
        $pdf->Cell(22.5,7,'Transmit',1);
        $pdf->Cell(22.5,7,'ON',1);
        $pdf->Cell(15,7,'Power',1);
        $pdf->Cell(15,7,'ON',1);
        $pdf->Cell(25,7,'1.Tx Problem',1);
        $pdf->Cell(35,7,'3.Interface Problem',1);
        $pdf->Cell(32,7,'5.Fisik Problem',1);
        $pdf->Cell(28,7,'7.Power Problem',1);
        $pdf->Ln();

        $pdf->Cell(45,7,'','RL',0);
        $pdf->Cell(22.5,7,'Receive',1);
        $pdf->Cell(52.5,7,'ON',1);
        $pdf->Cell(25,7,'2.Rx Problem',1);
        $pdf->Cell(35,7,'4.Connector Problem',1);
        $pdf->Cell(32,7,'6.Software Problem',1);
        $pdf->Cell(28,7,'8.Other',1);
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==6){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }elseif ($i==8){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }

          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==6){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');

  }

  function linkuji_pdf($filename,$data_pdf,$info) {
    $header = ['No','No Registrasi','LED Indicator','Gain','Power','Start-End','Result','KK'];
    $pdf = new FPDF('L', 'mm', 'A4');
    $lintas = new lintas();

    foreach ($data_pdf as $tgl => $data) :

        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(100);
        $pdf->Cell(40,10,'VSAT LINK (Modem,SSPA>10W,RFT)');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(30,7, 'Parameter Uji', 1);
        $pdf->Cell(120,7, 'Rules','LRTB',0,'C');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'Tidak Cacat/Mulus', 1);
        $pdf->Cell(30,7, '3.Gain', 1);
        $pdf->Cell(120,7, '10W-120W:(65dB s/d 85dB)', 'LRTB',0,'C');
        $pdf->Ln();

        $pdf->Cell(45,28, '2.LED Indicator', 1);
        $pdf->Cell(22.5,7, 'Online', 1);
        $pdf->Cell(22.5,7, 'ON', 1);
        $pdf->Cell(30,7, '4.Power', 1);
        $pdf->Cell(55,7, '10W: 40dBm 20W: 43dBm ', 'RTB',0,'C');
        $pdf->Cell(65,7, '40W:46dBm 60W: 49dBm 100W: 50dBm', 'LRTB',0,'C');
        $pdf->Ln();

        $pdf->Cell(45,7,'','RL',0);
        $pdf->Cell(22.5,7,'Transmit',1);
        $pdf->Cell(22.5,7,'ON',1);
        $pdf->Cell(150,7, 'Kategori Kerusakan (KK)','LRTB',0,'C');
        $pdf->Ln();

        $pdf->Cell(45,7,'','RL',0);
        $pdf->Cell(22.5,7,'Receive',1);
        $pdf->Cell(22.5,7,'ON',1);
        $pdf->Cell(30,7,'1.Tx Problem',1);
        $pdf->Cell(40,7,'3.Interface Problem',1);
        $pdf->Cell(40,7,'4.Connector Problem',1);
        $pdf->Cell(40,7,'7.Power Problem',1);
        $pdf->Ln();

        $pdf->Cell(45,7,'','RL',0);
        $pdf->Cell(22.5,7,'Fault (RFT)',1);
        $pdf->Cell(22.5,7,'OFF',1);
        $pdf->Cell(30,7,'2.Rx Problem',1);
        $pdf->Cell(40,7,'6.Software Problem',1);
        $pdf->Cell(40,7,'5.Fisik Problem',1);
        $pdf->Cell(40,7,'8.Other',1);
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==6){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }elseif ($i==8){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }

          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==6){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');

  }
  function bwa5uji_pdf($filename,$data_pdf,$info) {
    $header = ['No','No Registrasi','Factory Default','Fisik','Ping','Start-End','Result','KK'];
    $pdf = new FPDF('L','mm','A4');
    $lintas = new lintas();

    foreach ($data_pdf as $tgl => $data) :
        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(120);
        $pdf->Cell(40,10,'BWA5 (WiBas)');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(170,7, 'STANDAR NILAI UJI','LTB', 0, 'C');
        $pdf->Cell(70,7, 'Kategori Kerusakan (KK)','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(80,35, '*ip system WiBas:10.10.10.100', 'LRTB',0,'C');
        $pdf->Cell(35,7, '1.Tx Problem',0);
        $pdf->Cell(35,7, '6.Fisik Problem','R');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik',1);
        $pdf->Cell(45,7, 'Tidak Cacat/Mulus',1);
        $pdf->Cell(80,7, '','RL',0);
        $pdf->Cell(35,7, '2.Rx Problem',0);
        $pdf->Cell(35,7, '7.Software Problem','R');
        $pdf->Ln();

        $pdf->Cell(45,7, '2.Factory default',1);
        $pdf->Cell(45,7, 'Mandatory',1);
        $pdf->Cell(80,7, '','RL',0);
        $pdf->Cell(35,7, '3.Interface Problem',0);
        $pdf->Cell(35,7, '8.Other','R');
        $pdf->Ln();

        $pdf->Cell(45,7, '3.Ping ip system',1);
        $pdf->Cell(45,7, 'not RTO*',1);
        $pdf->Cell(80,7, '','RL',0);
        $pdf->Cell(35,7, '4.Power Problem',0);
        $pdf->Cell(35,7, '','R');
        $pdf->Ln();

        $pdf->Cell(45,7, '4.Software version',1);
        $pdf->Cell(45,7, '1,9',1);
        $pdf->Cell(80,7, '','RL',0);
        $pdf->Cell(35,7, '5.Connector Problem','B',0);
        $pdf->Cell(35,7, '','RB',0);
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==6){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }elseif ($i==8){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }

          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==6){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');

  }

  function bwa1uji_pdf($filename,$data_pdf,$info) {
    $header = ['No','No Registrasi','Power','Tx','RSL','BERT','Start-End','Result','KK'];
    $pdf = new FPDF('L','mm','A4');
    $lintas = new lintas();

    foreach ($data_pdf as $tgl => $data) :
        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(120);
        $pdf->Cell(40,10,'BWA1 (Remote)');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(30,7, 'Parameter Uji', 1);
        $pdf->Cell(50,7, 'Rules',1);
        $pdf->Cell(70,7, 'Kategori Kerusakan (KK)',1,'LRTB','C');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'OK', 1);
        $pdf->Cell(30,7, '4.Interface', 1);
        $pdf->Cell(50,7, 'Bert 300 second Free Error', 1);
        $pdf->Cell(35,7, '1.Tx Problem', 0);
        $pdf->Cell(35,7, '5.Connector Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '2.Power', 1);
        $pdf->Cell(45,7, 'On', 1);
        $pdf->Cell(30,7, '5.Tx', 1);
        $pdf->Cell(50,7, '-30 dBm s/d -25 dBm', 1);
        $pdf->Cell(35,7, '2.Rx Problem', 0);
        $pdf->Cell(35,7, '6.Fisik Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '3.Link', 1);
        $pdf->Cell(45,7, 'Connected', 1);
        $pdf->Cell(30,7, '5.Tx', 1);
        $pdf->Cell(50,7, '-84 dBm s/d -70 dBm', 1);
        $pdf->Cell(35,7, '3.Interface Problem', 0);
        $pdf->Cell(35,7, '7.Software Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '', 'LB');
        $pdf->Cell(45,7, '', 'B');
        $pdf->Cell(30,7, '', 'B');
        $pdf->Cell(50,7, '', 'RB');
        $pdf->Cell(35,7, '4.Power Problem', 'B');
        $pdf->Cell(35,7, '8.Other', 'RB');
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==6){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==8){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }elseif ($i==9){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }

          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==6){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==8){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');

  }

  function bwa1psuuji_pdf($filename,$data_pdf,$info) {
    $header = ['No','No Registrasi','Power','VDC (V)','VAC (mV)','Start-End','Result','KK'];
    $pdf = new FPDF ('L','mm','A4');
    $lintas = new lintas();

    foreach ($data_pdf as $tgl => $data) :
        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(110);
        $pdf->Cell(40,10,'BWA1 (Remote/PSU SAS)');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(150,7, 'Kategori Kerusakan (KK)',1,'LRTB','C');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'OK', 1);
        $pdf->Cell(75,7, '1.Tx Problem', 0);
        $pdf->Cell(75,7, '5.Connector Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '2.Power', 1);
        $pdf->Cell(45,7, 'On', 1);
        $pdf->Cell(75,7, '2.Rx Problem', 0);
        $pdf->Cell(75,7, '6.Fisik Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '3.Vout ( Vdc )', 1);
        $pdf->Cell(45,7, '-48.96Vdc s/d -47.04Vdc', 1);
        $pdf->Cell(75,7, '3.Interface Problem', 0);
        $pdf->Cell(75,7, '7.Software Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '4.Vout ( max )', 1);
        $pdf->Cell(45,7, '<= 360mV', 1);
        $pdf->Cell(75,7, '4.Power Problem', 'B');
        $pdf->Cell(75,7, '8.Other', 'RB');
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==6){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }elseif ($i==8){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }

          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==6){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');
  }
  function bwa2uji_pdf($filename,$data_pdf,$info) {
    $header = ['No','No Registrasi','Power','Tx','Rx','SNR/AVG','Start-End','Result','KK'];
    $pdf = new FPDF ('L','mm','A4');
    $lintas = new lintas();

    foreach ($data_pdf as $tgl => $data) :
        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(120);
        $pdf->Cell(40,10,'BWA2 (Remote)');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(30,7, 'Parameter Uji', 1);
        $pdf->Cell(50,7, 'Rules',1);
        $pdf->Cell(70,7, 'Kategori Kerusakan (KK)',1,'LRTB','C');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'OK', 1);
        $pdf->Cell(30,7, '4.Interface', 1);
        $pdf->Cell(50,7, 'Bert 300 second Free Error', 1);
        $pdf->Cell(35,7, '1.Tx Problem', 0);
        $pdf->Cell(35,7, '5.Connector Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '2.Power', 1);
        $pdf->Cell(45,7, 'On', 1);
        $pdf->Cell(30,7, '5.SNR', 1);
        $pdf->Cell(50,7, '>= 23dB', 1);
        $pdf->Cell(35,7, '2.Rx Problem', 0);
        $pdf->Cell(35,7, '6.Fisik Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '3.Link', 1);
        $pdf->Cell(45,7, 'Connected', 1);
        $pdf->Cell(30,7, '6.Tx/Rx', 1);
        $pdf->Cell(50,7, '<-30 / >-60', 1);
        $pdf->Cell(35,7, '3.Interface Problem', 0);
        $pdf->Cell(35,7, '7.Software Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '', 'LB');
        $pdf->Cell(45,7, '', 'B');
        $pdf->Cell(30,7, '', 'B');
        $pdf->Cell(50,7, '', 'RB');
        $pdf->Cell(35,7, '4.Power Problem', 'B');
        $pdf->Cell(35,7, '8.Other', 'RB');
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==6){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==8){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }elseif ($i==9){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }

          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==6){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==8){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');
  }

  function bwa3articonetuji_pdf($filename,$data_pdf,$info) {
    $header = ['No','No Registrasi','Power','Ethernet','UL/DL SNR','UL/DL Rssi','Start-End','Result','KK'];
    $pdf = new FPDF ('L','mm','A4');
    $lintas = new lintas();

    foreach ($data_pdf as $tgl => $data) :
        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(110);
        $pdf->Cell(40,10,'BWA3 Articonet (Remote)');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(30,7, 'Parameter Uji', 1);
        $pdf->Cell(50,7, 'Rules',1);
        $pdf->Cell(70,7, 'Kategori Kerusakan (KK)',1,'LRTB','C');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'OK', 1);
        $pdf->Cell(30,7, '4.Ethernet', 1);
        $pdf->Cell(50,7, 'UP, Test ping Reply', 1);
        $pdf->Cell(35,7, '1.Tx Problem', 0);
        $pdf->Cell(35,7, '5.Connector Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '2.Power', 1);
        $pdf->Cell(45,7, 'On', 1);
        $pdf->Cell(30,7, '5.UL/DL SNR', 1);
        $pdf->Cell(50,7, '> 23dB', 1);
        $pdf->Cell(35,7, '2.Rx Problem', 0);
        $pdf->Cell(35,7, '6.Fisik Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '3.Link', 1);
        $pdf->Cell(45,7, 'Connected', 1);
        $pdf->Cell(30,7, '6.UL/DL Rssi', 1);
        $pdf->Cell(50,7, '> -85dBm', 1);
        $pdf->Cell(35,7, '3.Interface Problem', 0);
        $pdf->Cell(35,7, '7.Software Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '', 'LB');
        $pdf->Cell(45,7, '', 'B');
        $pdf->Cell(30,7, '', 'B');
        $pdf->Cell(50,7, '', 'RB');
        $pdf->Cell(35,7, '4.Power Problem', 'B');
        $pdf->Cell(35,7, '8.Other', 'RB');
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==6){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==8){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }elseif ($i==9){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }

          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==6){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==8){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');
  }

  function bwa3hariffuji_pdf($filename,$data_pdf,$info) {
    $header = ['No','No Registrasi','Power','Ethernet','CINR','Rssi','Start-End','Result','KK'];
    $pdf = new FPDF ('L','mm','A4');
    $lintas = new lintas();

    foreach ($data_pdf as $tgl => $data) :
        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(110);
        $pdf->Cell(40,10,'BWA3 Hariff (Remote)');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(30,7, 'Parameter Uji', 1);
        $pdf->Cell(50,7, 'Rules',1);
        $pdf->Cell(70,7, 'Kategori Kerusakan (KK)',1,'LRTB','C');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'OK', 1);
        $pdf->Cell(30,7, '4.Ethernet', 1);
        $pdf->Cell(50,7, 'UP, Test ping Reply', 1);
        $pdf->Cell(35,7, '1.Tx Problem', 0);
        $pdf->Cell(35,7, '5.Connector Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '2.Power', 1);
        $pdf->Cell(45,7, 'On', 1);
        $pdf->Cell(30,7, '5.CINR', 1);
        $pdf->Cell(50,7, '> 23dBm', 1);
        $pdf->Cell(35,7, '2.Rx Problem', 0);
        $pdf->Cell(35,7, '6.Fisik Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '3.Link', 1);
        $pdf->Cell(45,7, 'Connected', 1);
        $pdf->Cell(30,7, '6.Rssi', 1);
        $pdf->Cell(50,7, '> -85dBm', 1);
        $pdf->Cell(35,7, '3.Interface Problem', 0);
        $pdf->Cell(35,7, '7.Software Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '', 'LB');
        $pdf->Cell(45,7, '', 'B');
        $pdf->Cell(30,7, '', 'B');
        $pdf->Cell(50,7, '', 'RB');
        $pdf->Cell(35,7, '4.Power Problem', 'B');
        $pdf->Cell(35,7, '8.Other', 'RB');
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==6){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==8){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }elseif ($i==9){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }

          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==6){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==8){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');

  }

  function routeruji_pdf($filename,$data_pdf,$info) {
     $header = ['No','No Registrasi','Fisik','Power','Link','Interface','Ethernet','Start-End','Result','KK'];
     $pdf = new FPDF ('L','mm','A4');
     $lintas = new lintas();

     foreach ($data_pdf as $tgl => $data) :
        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(70);
        $pdf->Cell(40,10,'Router (Cisco,Mikronet,Maipu,OneAcces,Adtran,Raisecom)');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(30,7, 'Parameter Uji', 1);
        $pdf->Cell(50,7, 'Rules',1);
        $pdf->Cell(70,7, 'Kategori Kerusakan (KK)',1,'LRTB','C');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'OK', 1);
        $pdf->Cell(30,7, '4.Interface', 1);
        $pdf->Cell(50,7, 'Serial Up, line protocol Up', 1);
        $pdf->Cell(35,7, '1.Tx Problem', 0);
        $pdf->Cell(35,7, '5.Connector Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '2.Power', 1);
        $pdf->Cell(45,7, 'On', 1);
        $pdf->Cell(30,7, '5.Tx', 1);
        $pdf->Cell(50,7, 'Up, Test ping reply', 1);
        $pdf->Cell(35,7, '2.Rx Problem', 0);
        $pdf->Cell(35,7, '6.Fisik Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '3.Link', 1);
        $pdf->Cell(45,7, 'Connected', 1);
        $pdf->Cell(30,7, '6.Rsl', 1);
        $pdf->Cell(50,7, 'Voice On', 1);
        $pdf->Cell(35,7, '3.Interface Problem', 0);
        $pdf->Cell(35,7, '7.Software Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '', 'LB');
        $pdf->Cell(45,7, '', 'B');
        $pdf->Cell(30,7, '', 'B');
        $pdf->Cell(50,7, '', 'RB');
        $pdf->Cell(35,7, '4.Power Problem', 'B');
        $pdf->Cell(35,7, '8.Other', 'RB');
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==6){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }  elseif ($i==8){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==7){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==9){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          } elseif ($i==10){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }
          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(20,7, $col, 1);
            }  elseif ($i==6){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(30,7, $col, 1);
            }    elseif ($i==8){
                $pdf->Cell(40,7, $col, 1);
            } elseif ($i==9){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');
  }

  function radiolinkuji_pdf ($filename,$data_pdf,$info) {
    $header = ['No','No Registrasi','Power','Tx','Rx','Bert','Start-End','Result','KK'];
    $pdf =  new FPDF ('L','mm','A4');
    $lintas = new lintas();

    foreach ($data_pdf as $tgl => $data) :
        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(125);
        $pdf->Cell(40,10,'Radio Link');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(30,7, 'Parameter Uji', 1);
        $pdf->Cell(50,7, 'Rules',1);
        $pdf->Cell(70,7, 'Kategori Kerusakan (KK)',1,'LRTB','C');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'OK', 1);
        $pdf->Cell(30,7, '4.Interface', 1);
        $pdf->Cell(50,7, 'Bert 300 second Free Error', 1);
        $pdf->Cell(35,7, '1.Tx Problem', 0);
        $pdf->Cell(35,7, '5.Connector Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '2.Power', 1);
        $pdf->Cell( 45,7, 'On', 1);
        $pdf->Cell(30,7, '5.Tx Power', 1);
        $pdf->Cell(50,7, '-15 s/d 25dBm', 1);
        $pdf->Cell(35,7, '2.Rx Problem', 0);
        $pdf->Cell(35,7, '6.Fisik Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '3.Link', 1);
        $pdf->Cell(45,7, 'Connected', 1);
        $pdf->Cell(30,7, '6.Rx', 1);
        $pdf->Cell(50,7, '>-65 dBm', 1);
        $pdf->Cell(35,7, '3.Interface Problem', 0);
        $pdf->Cell(35,7, '7.Software Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '', 'LB');
        $pdf->Cell(45,7, '', 'B');
        $pdf->Cell(30,7, '', 'B');
        $pdf->Cell(50,7, '', 'RB');
        $pdf->Cell(35,7, '4.Power Problem', 'B');
        $pdf->Cell(35,7, '8.Other', 'RB');
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==6){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==8){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }elseif ($i==9){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }

          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==6){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==8){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

        $pdf->Ln();
        $pdf->Cell(50,7, "Tester", 0);
        $pdf->Cell(150,7, "", 0);
        $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');
  }
  function wirelineuji_pdf ($filename,$data_pdf,$info) {
    $header = ['No','No Registrasi','Power','Link','BERT','Start-End','Result','KK'];
    $pdf = new FPDF ('L','mm','A4');
    $lintas = new lintas();

    foreach ($data_pdf as $tgl => $data) :
        $pdf->AddPage();

        $pdf->Image('pic/logo.png',10,0,50);
        $pdf->SetFont('Arial','B',16);
        $pdf->Cell(100); //geser ke kenan
        $pdf->Cell(40,10,'HASIL PENGUJIAN PERANGKAT','C');
        $pdf->Ln();
        $pdf->Cell(125);
        $pdf->Cell(40,10,'Radio Link');

        $pdf->Ln(17);
        $pdf->SetFont('Arial','',10);

        $this->set_info($info, $tgl, $pdf, $lintas);

        $pdf->Ln();
        $pdf->Ln();

        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(240,7, 'STANDAR NILAI UJI','LRTB', 0, 'C');
        $pdf->Ln();
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(45,7, 'Parameter Uji', 1);
        $pdf->Cell(45,7, 'Rules', 1);
        $pdf->Cell(150,7, 'Kategori Kerusakan (KK)',1,'LRTB','C');
        $pdf->Ln();

        $pdf->Cell(45,7, '1.Fisik', 1);
        $pdf->Cell(45,7, 'OK', 1);
        $pdf->Cell(75,7, '1.Tx Problem', 0);
        $pdf->Cell(75,7, '5.Connector Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '2.Power', 1);
        $pdf->Cell(45,7, 'On', 1);
        $pdf->Cell(75,7, '2.Rx Problem', 0);
        $pdf->Cell(75,7, '6.Fisik Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '3.Link', 1);
        $pdf->Cell(45,7, 'Connected', 1);
        $pdf->Cell(75,7, '3.Interface Problem', 0);
        $pdf->Cell(75,7, '7.Software Problem', 'R');
        $pdf->Ln();

        $pdf->Cell(45,7, '4.BERT', 1);
        $pdf->Cell(45,7, 'Bert 300 second Free Error', 1);
        $pdf->Cell(75,7, '4.Power Problem', 'B');
        $pdf->Cell(75,7, '8.Other', 'RB');
        $pdf->Ln();

        $pdf->Ln();
        $pdf->SetFont('Arial', null ,9);
        $i = 1;
        foreach ($header as $key => $value) {
          if ($i== 1) {
            $pdf->Cell(10,7, $value,'LRTB',0,'C');
          }  elseif ($i==2){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==3){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==4){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          } elseif ($i==5){
            $pdf->Cell(30,7, $value,'LRTB',0,'C');
          }  elseif ($i==6){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }  elseif ($i==7){
            $pdf->Cell(40,7, $value,'LRTB',0,'C');
          }elseif ($i==8){
            $pdf->Cell(20,7, $value,'LRTB',0,'C');
          }

          $i++;
        }
        $pdf->Ln();

        $no = 1;
        foreach ($data as $row) {
         $i = 1;
         $pdf->Cell(10,7, $no, 1);
         foreach ($row as $col) {
            if ($i==1) {
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==2){
                $pdf->Cell(30,7, $col, 1);
            }  elseif ($i==3){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==4){
                $pdf->Cell(30,7, $col, 1);
            } elseif ($i==5){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==6){
                $pdf->Cell(40,7, $col, 1);
            }  elseif ($i==7){
                $pdf->Cell(20,7, $col, 1);
            }
          $i++;
           }
           $pdf->Ln();
          $no++;
        }

    $pdf->Ln();
    $pdf->Cell(50,7, "Tester", 0);
    $pdf->Cell(150,7, "", 0);
    $pdf->Cell(50,7, "Verifikator", 0);
    endforeach;
    $pdf->Output($filename, 'D');

  }
}

?>
